/*
 Navicat Premium Data Transfer

 Source Server         : 10.47.150.143 Brili
 Source Server Type    : MySQL
 Source Server Version : 50632
 Source Host           : 10.47.150.143:3306
 Source Schema         : inventory_ttc

 Target Server Type    : MySQL
 Target Server Version : 50632
 File Encoding         : 65001

 Date: 23/07/2019 10:30:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for adt_pemakaian_energi_item
-- ----------------------------
DROP TABLE IF EXISTS `adt_pemakaian_energi_item`;
CREATE TABLE `adt_pemakaian_energi_item`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kategori_item` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_item` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `satuan_item` char(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 33 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_pemakaian_energi_item
-- ----------------------------
INSERT INTO `adt_pemakaian_energi_item` VALUES (1, 'ttc_bsd', 'DAYA PLN', 'TRAFO 1', 'kVA', '2019-07-02 11:20:11');
INSERT INTO `adt_pemakaian_energi_item` VALUES (2, 'ttc_bsd', 'DAYA PLN', 'TRAFO 2', 'kVA', '2019-07-02 11:20:25');
INSERT INTO `adt_pemakaian_energi_item` VALUES (3, 'ttc_bsd', 'DAYA PLN', 'TRAFO 3', 'kVA', '2019-07-02 11:20:40');
INSERT INTO `adt_pemakaian_energi_item` VALUES (4, 'ttc_bsd', 'GENERATOR SET', 'GENERATOR SET', 'kVA', '2019-07-02 11:21:12');
INSERT INTO `adt_pemakaian_energi_item` VALUES (5, 'ttc_bsd', 'BBM SOLAR GENSET', 'BBM SOLAR GENSET', 'Liter', '2019-07-02 11:21:56');
INSERT INTO `adt_pemakaian_energi_item` VALUES (6, 'ttc_bsd', 'PEMAKAIAN AIR', 'PEMAKAIAN AIR', 'm3', '2019-07-02 11:22:34');
INSERT INTO `adt_pemakaian_energi_item` VALUES (7, 'ttc_bsd', 'PEMAKAIAN LISTRIK', 'WBP', 'kwh', '2019-07-02 11:22:59');
INSERT INTO `adt_pemakaian_energi_item` VALUES (8, 'ttc_bsd', 'PEMAKAIAN LISTRIK', 'LWBP', 'kwh', '2019-07-02 11:23:03');
INSERT INTO `adt_pemakaian_energi_item` VALUES (9, 'ttc_tbs', 'DAYA PLN', 'TRAFO 1', 'kVA', '2019-07-02 11:20:11');
INSERT INTO `adt_pemakaian_energi_item` VALUES (10, 'ttc_tbs', 'DAYA PLN', 'TRAFO 2', 'kVA', '2019-07-02 11:20:25');
INSERT INTO `adt_pemakaian_energi_item` VALUES (11, 'ttc_tbs', 'DAYA PLN', 'TRAFO 3', 'kVA', '2019-07-02 11:20:40');
INSERT INTO `adt_pemakaian_energi_item` VALUES (12, 'ttc_tbs', 'GENERATOR SET', 'GENERATOR SET', 'kVA', '2019-07-02 11:21:12');
INSERT INTO `adt_pemakaian_energi_item` VALUES (13, 'ttc_tbs', 'BBM SOLAR GENSET', 'BBM SOLAR GENSET', 'Liter', '2019-07-02 11:21:56');
INSERT INTO `adt_pemakaian_energi_item` VALUES (14, 'ttc_tbs', 'PEMAKAIAN AIR', 'PEMAKAIAN AIR', 'm3', '2019-07-02 11:22:34');
INSERT INTO `adt_pemakaian_energi_item` VALUES (15, 'ttc_tbs', 'PEMAKAIAN LISTRIK', 'WBP', 'kwh', '2019-07-02 11:22:59');
INSERT INTO `adt_pemakaian_energi_item` VALUES (16, 'ttc_tbs', 'PEMAKAIAN LISTRIK', 'LWBP', 'kwh', '2019-07-02 11:23:03');
INSERT INTO `adt_pemakaian_energi_item` VALUES (17, 'ttc_buaran', 'DAYA PLN', 'TRAFO 1', 'kVA', '2019-07-02 11:20:11');
INSERT INTO `adt_pemakaian_energi_item` VALUES (18, 'ttc_buaran', 'DAYA PLN', 'TRAFO 2', 'kVA', '2019-07-02 11:20:25');
INSERT INTO `adt_pemakaian_energi_item` VALUES (19, 'ttc_buaran', 'DAYA PLN', 'TRAFO 3', 'kVA', '2019-07-02 11:20:40');
INSERT INTO `adt_pemakaian_energi_item` VALUES (20, 'ttc_buaran', 'GENERATOR SET', 'GENERATOR SET', 'kVA', '2019-07-02 11:21:12');
INSERT INTO `adt_pemakaian_energi_item` VALUES (21, 'ttc_buaran', 'BBM SOLAR GENSET', 'BBM SOLAR GENSET', 'Liter', '2019-07-02 11:21:56');
INSERT INTO `adt_pemakaian_energi_item` VALUES (22, 'ttc_buaran', 'PEMAKAIAN AIR', 'PEMAKAIAN AIR', 'm3', '2019-07-02 11:22:34');
INSERT INTO `adt_pemakaian_energi_item` VALUES (23, 'ttc_buaran', 'PEMAKAIAN LISTRIK', 'WBP', 'kwh', '2019-07-02 11:22:59');
INSERT INTO `adt_pemakaian_energi_item` VALUES (24, 'ttc_buaran', 'PEMAKAIAN LISTRIK', 'LWBP', 'kwh', '2019-07-02 11:23:03');
INSERT INTO `adt_pemakaian_energi_item` VALUES (25, 'ttc_meruya', 'DAYA PLN', 'TRAFO 1', 'kVA', '2019-07-02 11:20:11');
INSERT INTO `adt_pemakaian_energi_item` VALUES (26, 'ttc_meruya', 'DAYA PLN', 'TRAFO 2', 'kVA', '2019-07-02 11:20:25');
INSERT INTO `adt_pemakaian_energi_item` VALUES (27, 'ttc_meruya', 'DAYA PLN', 'TRAFO 3', 'kVA', '2019-07-02 11:20:40');
INSERT INTO `adt_pemakaian_energi_item` VALUES (28, 'ttc_meruya', 'GENERATOR SET', 'GENERATOR SET', 'kVA', '2019-07-02 11:21:12');
INSERT INTO `adt_pemakaian_energi_item` VALUES (29, 'ttc_meruya', 'BBM SOLAR GENSET', 'BBM SOLAR GENSET', 'Liter', '2019-07-02 11:21:56');
INSERT INTO `adt_pemakaian_energi_item` VALUES (30, 'ttc_meruya', 'PEMAKAIAN AIR', 'PEMAKAIAN AIR', 'm3', '2019-07-02 11:22:34');
INSERT INTO `adt_pemakaian_energi_item` VALUES (31, 'ttc_meruya', 'PEMAKAIAN LISTRIK', 'WBP', 'kwh', '2019-07-02 11:22:59');
INSERT INTO `adt_pemakaian_energi_item` VALUES (32, 'ttc_meruya', 'PEMAKAIAN LISTRIK', 'LWBP', 'kwh', '2019-07-02 11:23:03');

SET FOREIGN_KEY_CHECKS = 1;
