/*
 Navicat Premium Data Transfer

 Source Server         : LOCALHOST
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : automation_ttc

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 03/07/2019 11:54:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for adt_log
-- ----------------------------
DROP TABLE IF EXISTS `adt_log`;
CREATE TABLE `adt_log`  (
  `id_log` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_log` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `action` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reference` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `other_info` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datetime_log` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_log`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_log
-- ----------------------------
INSERT INTO `adt_log` VALUES (1, 'login', 'admin', 'login from ::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, NULL);
INSERT INTO `adt_log` VALUES (2, 'login', 'admin', 'login from ::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, NULL);
INSERT INTO `adt_log` VALUES (3, 'login', 'admin', 'login from ::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, NULL);

-- ----------------------------
-- Table structure for adt_master_ttc
-- ----------------------------
DROP TABLE IF EXISTS `adt_master_ttc`;
CREATE TABLE `adt_master_ttc`  (
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_ttc` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat_ttc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pic_ttc` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `b0_pic_ttc` varchar(14) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_ttc`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_master_ttc
-- ----------------------------
INSERT INTO `adt_master_ttc` VALUES ('ttc_bsd', 'TTC BSD', 'jln bsd', 'Ferry Swasono', '62811725511');
INSERT INTO `adt_master_ttc` VALUES ('ttc_buaran', 'TTC Buaran', 'Buaran', 'Aris Ismujoko', '628111891638');
INSERT INTO `adt_master_ttc` VALUES ('ttc_meruya', 'TTC Meruya', 'Meruya', 'Chairul Mustakim', '62811100251');
INSERT INTO `adt_master_ttc` VALUES ('ttc_tbs', 'TTC TBS', 'TB Simatupang', 'Budi Prihartanto', '62811871568');

-- ----------------------------
-- Table structure for adt_master_user
-- ----------------------------
DROP TABLE IF EXISTS `adt_master_user`;
CREATE TABLE `adt_master_user`  (
  `username` char(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fullname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` char(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `privilege` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_master_user
-- ----------------------------
INSERT INTO `adt_master_user` VALUES ('admin', 'Administrator', '$2y$10$oTEQjYfWmkutUzY6z.6fluyxJmM3XHnRhZepHsbU2byk.gikKCoji', 'admin', 'ttc_bsd', '2019-07-02 09:07:58');

-- ----------------------------
-- Table structure for adt_pemakaian_energi
-- ----------------------------
DROP TABLE IF EXISTS `adt_pemakaian_energi`;
CREATE TABLE `adt_pemakaian_energi`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_item` int(11) UNSIGNED NULL DEFAULT NULL,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kapasitas` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pemakaian` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `persentase` double(5, 2) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `bulan` char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun` year NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 66 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_pemakaian_energi
-- ----------------------------
INSERT INTO `adt_pemakaian_energi` VALUES (9, 1, 'ttc_bsd', '100', '50', 50.00, 'ok', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (10, 2, 'ttc_bsd', '200', '35', 43.00, 'pemakian biasa', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (11, 3, 'ttc_bsd', '399', '34', 45.00, 'pemakian biasa', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (12, 4, 'ttc_bsd', '35', '457', 45.00, 'pemakian biasa', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (13, 5, 'ttc_bsd', '35', '67', 45.00, 'pemakian biasa', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (14, 6, 'ttc_bsd', '23', '54', 65.00, 'pemakian biasa', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (15, 7, 'ttc_bsd', '545', '45', 54.00, 'pemakian biasa', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (16, 8, 'ttc_bsd', '4646', '65', 45.00, 'pemakian biasa', '01', 2019, '2019-07-02 16:14:23');
INSERT INTO `adt_pemakaian_energi` VALUES (17, 1, 'ttc_bsd', '1', '2', 3.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (18, 2, 'ttc_bsd', '4', '5', 6.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (19, 3, 'ttc_bsd', '7', '8', 9.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (20, 4, 'ttc_bsd', '10', '11', 12.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (21, 5, 'ttc_bsd', '13', '14', 15.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (22, 6, 'ttc_bsd', '16', '17', 18.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (23, 7, 'ttc_bsd', '19', '20', 21.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (24, 8, 'ttc_bsd', '22', '23', 24.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (25, 9, 'ttc_bsd', '25', '26', 27.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (26, 1, 'ttc_bsd', '35', '32', 42.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (27, 2, 'ttc_bsd', '34', '65', 23.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (28, 3, 'ttc_bsd', '54', '23', 65.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (29, 4, 'ttc_bsd', '23', '65', 45.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (30, 5, 'ttc_bsd', '34', '34', 23.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (31, 6, 'ttc_bsd', '52', '32', 54.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (32, 7, 'ttc_bsd', '45', '55', 345.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (33, 8, 'ttc_bsd', '23', '45', 54.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (34, 1, 'ttc_bsd', '34', '65', 23.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (35, 2, 'ttc_bsd', '54', '65', 34.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (36, 3, 'ttc_bsd', '45', '65', 34.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (37, 4, 'ttc_bsd', '65', '34', 65.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (38, 5, 'ttc_bsd', '54', '23', 34.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (39, 6, 'ttc_bsd', '54', '23', 54.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (40, 7, 'ttc_bsd', '54', '23', 23.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (41, 8, 'ttc_bsd', '54', '23', 54.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (42, 1, 'ttc_bsd', '54', '23', 54.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (43, 2, 'ttc_bsd', '54', '23', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (44, 3, 'ttc_bsd', '65', '54', 54.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (45, 4, 'ttc_bsd', '34', '45', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (46, 5, 'ttc_bsd', '34', '65', 65.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (47, 6, 'ttc_bsd', '25', '65', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (48, 7, 'ttc_bsd', '45', '45', 65.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (49, 8, 'ttc_bsd', '45', '65', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (50, 1, 'ttc_bsd', '54', '65', 45.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (51, 2, 'ttc_bsd', '54', '34', 65.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (52, 3, 'ttc_bsd', '34', '65', 65.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (53, 4, 'ttc_bsd', '65', '54', 34.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (54, 5, 'ttc_bsd', '34', '65', 34.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (55, 6, 'ttc_bsd', '54', '23', 54.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (56, 7, 'ttc_bsd', '65', '45', 65.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (57, 8, 'ttc_bsd', '34', '54', 34.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (58, 1, 'ttc_bsd', '46', '34', 34.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (59, 2, 'ttc_bsd', '32', '34', 34.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (60, 3, 'ttc_bsd', '34', '43', 54.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (61, 4, 'ttc_bsd', '23', '54', 23.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (62, 5, 'ttc_bsd', '23', '54', 54.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (63, 6, 'ttc_bsd', '23', '54', 23.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (64, 7, 'ttc_bsd', '54', '54', 23.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (65, 8, 'ttc_bsd', '22', '35', 54.00, '-', '07', 2019, '2019-07-03 11:53:26');

-- ----------------------------
-- Table structure for adt_pemakaian_energi_item
-- ----------------------------
DROP TABLE IF EXISTS `adt_pemakaian_energi_item`;
CREATE TABLE `adt_pemakaian_energi_item`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kategori_item` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_item` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `satuan_item` char(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_pemakaian_energi_item
-- ----------------------------
INSERT INTO `adt_pemakaian_energi_item` VALUES (1, 'ttc_bsd', 'DAYA PLN', 'TRAFO 1', 'kVA', '2019-07-02 11:20:11');
INSERT INTO `adt_pemakaian_energi_item` VALUES (2, 'ttc_bsd', 'DAYA PLN', 'TRAFO 2', 'kVA', '2019-07-02 11:20:25');
INSERT INTO `adt_pemakaian_energi_item` VALUES (3, 'ttc_bsd', 'DAYA PLN', 'TRAFO 3', 'kVA', '2019-07-02 11:20:40');
INSERT INTO `adt_pemakaian_energi_item` VALUES (4, 'ttc_bsd', 'GENERATOR SET', 'GENERATOR SET', 'kVA', '2019-07-02 11:21:12');
INSERT INTO `adt_pemakaian_energi_item` VALUES (5, 'ttc_bsd', 'BBM SOLAR GENSET', 'BBM SOLAR GENSET', 'Liter', '2019-07-02 11:21:56');
INSERT INTO `adt_pemakaian_energi_item` VALUES (6, 'ttc_bsd', 'PEMAKAIAN AIR', 'PEMAKAIAN AIR', 'm3', '2019-07-02 11:22:34');
INSERT INTO `adt_pemakaian_energi_item` VALUES (7, 'ttc_bsd', 'PEMAKAIAN LISTRIK', 'WBP', 'kwh', '2019-07-02 11:22:59');
INSERT INTO `adt_pemakaian_energi_item` VALUES (8, 'ttc_bsd', 'PEMAKAIAN LISTRIK', 'LWBP', 'kwh', '2019-07-02 11:23:03');

-- ----------------------------
-- Table structure for adt_pemakian_solar
-- ----------------------------
DROP TABLE IF EXISTS `adt_pemakian_solar`;
CREATE TABLE `adt_pemakian_solar`  (
  `id_pemakaian_solar` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uraian_kerja` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal_pemakaian` date NULL DEFAULT NULL,
  `liter_pemakaian` double(11, 2) NULL DEFAULT NULL,
  `backup_time` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_by` char(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pemakaian_solar`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for adt_perizinan_gedung
-- ----------------------------
DROP TABLE IF EXISTS `adt_perizinan_gedung`;
CREATE TABLE `adt_perizinan_gedung`  (
  `id_perizinan` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_perizinan` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status_perizinan` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun_pembuatan` date NULL DEFAULT NULL,
  `pemeriksaan1` date NULL DEFAULT NULL,
  `pemeriksaan2` date NULL DEFAULT NULL,
  `masa_berlaku1` date NULL DEFAULT NULL,
  `masa_berlaku2` date NULL DEFAULT NULL,
  `lama_masa_perizinan` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_perizinan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
