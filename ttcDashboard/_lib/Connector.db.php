<?php debug_backtrace() || die ("Direct access not permitted");
date_default_timezone_set('Asia/Jakarta');
    class Db {
        public $config = array(
            'server_db'=>array(
                'host'=>'127.0.0.1',
                'user'=>'root',
                'pass'=>''
            )
            // 'server_db'=>array(
            //     'host'=>'10.47.150.143',
            //     'user'=>'brilianpra',
            //     'pass'=>'April2018'
            // )
        );
        
        public function connect($server,$db){
            $config = (object) @$this->config[$server];
            if(!isset($this->config[$server]) || !$this->config[$server]){
                echo 'ERROR: Config not found';
                exit();
            }
            mysqli_report(MYSQLI_REPORT_STRICT);
            try {
                return new mysqli($config->host,$config->user,$config->pass,$db);
                //echo 'connect success';
            } catch (Exception $e) {
                echo 'ERROR:'.$e->getMessage();
                exit();
            }
            
        }
    }
?>
