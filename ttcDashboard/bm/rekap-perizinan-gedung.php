
<?php
session_start();
require_once "../global.config.php";
if (!isset($_SESSION[$auth_name])) {
    header('Location: ' . base_url('login.php'));
}
$title = "Index";
$auth = (object) $_SESSION[$auth_name];
require_once "../component/header.php";
require_once "../_lib/Connector.db.php";

$db = new Db();
$mysqli = $db->connect('server_db', $db_name);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-content">
                <div>
                    <h3 class="pull-left">Rekap Perizinan Gedung</h3>
                    <div class="pull-right">
                        <input type="text" class="tahun" value="<?=date('Y');?>" /><button
                            class="btn_filter_tahun">Filter</button>
                    </div>
                    <div id="container_body" style="clear:both;min-width: 310px; height: 50px; margin: 0 auto">
                        
                            <div class="form-group">
                    <button type="button" class="btn btn-sm btn-info btn_tambah_data" data-toggle="modal" data-target="#modalLoginForm"
                        data-url="<?=base_url('backend/rekap-perizinan-gedung.php');?>">Input Data</button>
                    <!-- <button type="button" class="btn btn-sm btn-info btn_tambah_data" data-target="#tableData" id="showData">show / hide data</button> -->
                </div> 
            </div>
          <div id="tabel"></div>
           
<?php require_once "../component/footer.php";?>
<!--MODAL AREA TAMBAH DATA-->
<div class="modal inmodal" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInLeft" style="width:50% ; margin-left:25%" >
            <div class="modal-header">
        <h4 class="modal-title w-100 font-weight-bold">Tambah Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-1">

        <div class="md-form mb-1">
        <form id="formInputPerizinanGedung" method="post"
                    action="<?=base_url('backend/rekap-perizinan-gedung.php?act=simpan');?>">
                    <table class="table table-sm">

          <label>id_ttc *</label>
          <input type="text" id="id_ttc" name="id_ttc"  class="form-control validate" required="true" >
        </div>

        <div class="md-form mb-4">
         
          <label>Nama Perizinan *</label>
          <input type="text" id="nama_perizinan" name="nama_perizinan"  class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>Status Izin *</label>
          <input type="text" id="status_perizinan" name="status_perizinan" class="form-control validate" required="true" >
        </div>

        <div class="md-form mb-4">
         
          <label>Tahun Pembuatan *</label>
          <input type="date" id="tahun_pembuatan" name="tahun_pembuatan" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>pemeriksaan 1*</label>
          <input type="date" id="pemeriksaan1" name="pemeriksaan1" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>pemeriksaan 2*</label>
          <input type="date" id="pemeriksaan2" name="pemeriksaan2" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>masa berlaku 1*</label>
          <input type="date" id="masa_berlaku1" name="masa_berlaku1" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>masa berlaku 2*</label>
          <input type="date" id="masa_berlaku2" name="masa_berlaku2" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>lama masa perizinan*</label>
          <input type="text" id="lama_masa_perizinan" name="lama_masa_perizinan" class="form-control validate" required="true">
        </div>

      </div>
      </table>
      <div class="modal-footer d-flex justify-content-center">
        <button type="submit" class="btn btn-default" id="submit" value="submit">submit</button>
        <button type="reset" class="btn btn-default" id="reset">reset</button>
      </div>
      </form>
    </div>
  </div>
</div>

<?php require_once "../component/assets_js.php";?>
<script src="<?=base_url();?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/export-data.js"></script>

<script type="text/javascript">
    $('document').ready(function () {
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $(".bulan_tahun").datepicker({
            format: "mm-yyyy",
            startView: "months",
            viewMode: 1,
            minViewMode: 1,
            autoclose: true
        });
        $(".tahun").datepicker({
            format: "yyyy",
            startView: "years",
            viewMode: 1,
            minViewMode: 2,
            autoclose: true
        });

    });

        $(document).on('submit', 'form#formInputPerizinanGedung', function (e) {
            e.preventDefault();
            var form = $(this);
            var data = new FormData(form[0]);
            jQuery.ajax({
                url: form.attr('action'),
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST',
                success: function (data) {

                  if (data.status == 1) {
                    location.reload();
                    } 
                    alert(data.message);

                },
                error: function (e) {
                    alert(e.message);
                }
            });
        });

        $(document).ready(function() {

		    	$.ajax({
	            type : "POST",
	            url   : '../backend/rekap-perizinan-gedung.php?act=show_data',
	            async : false,
                dataType : 'json',
	            success : function(data){
        
                var html = '';

                html = "<table border='0' class='table table-bordered table-striped table-hover' id='tableData' style=' text-align:center;'>";
                html += ` <tr>
                                                <th class="success" rowspan="3" style="padding-top : 40px;"><center>No</th>
                                                <th td class="active" rowspan="3" style="padding-top : 40px;"><center>NAMA PERIZINAN</th>
                                                <th colspan="6"><center>TTC BSD</th>
                                                <th class="active" rowspan= "3" style="padding-top : 40px;"><center>LAMANYA MASA PERIZINAN</th>
                                                <th class="info" rowspan= "3" style="padding-top : 40px;"><center>ACTION</th>
                                                </tr>`;

                html+= `<tr>
                                                <th class="info" rowspan="2" style="padding-top : 25px;"><center>STATUS IZIN</th>
                                                <th <td class="warning" rowspan="2" style="padding-top : 25px;"><center>TAHUN PEMBUATAN</th>
                                                <th class="success" colspan="2"><center>PEMERIKSAAN PENGUJIAN BERKALA</th>
                                                <th class="info" colspan="2" style="padding-top : 15px;"><center>MASA BERLAKU</th>
                                                </tr>`;

                html+= `<tr>
                                                <th class="success"><center>I</th>
                                                <th class="success"><center>II</th>
                                                <th class="info"><center>I</th>
                                                <th class="info"><center>II</th>
                                              
                         </tr>`;

               var dataset = data.result;
              for(var i=0; i<dataset.length; i++){

              	html += '<tr class="warning">'+'No'+'</tr>';
              	html += '<td class="success">'+(i+1)+'</td>';
                html += '<td class="active"  name="np">'+dataset[i].nama_perizinan+ '</td>';
                html += '<td class="info"  name="sp">'+dataset[i].status_perizinan+'</td>';
                html += '<td class="warning"  name="tp">'+dataset[i].tahun_pembuatan+ '</td>';
                html += '<td class="success" name="p1">'+dataset[i].pemeriksaan1+ '</td>';

                html += '<td class="success"  name="p2">'+dataset[i].pemeriksaan2+ '</td>';
                html += '<td class="info"  name="mb1">'+dataset[i].masa_berlaku1+'</td>';
                html += '<td class="info" name="mb2">'+dataset[i].masa_berlaku2+ '</td>';
                html += '<td class="active"  name="lmp">'+dataset[i].lama_masa_perizinan+ '</td>';
              	html += '<td><a href="#" it="'+dataset[i].id_ttc+'" np="'+dataset[i].nama_perizinan+'" sp="'+dataset[i].status_perizinan+'" tp="'+dataset[i].tahun_pembuatan+'" p1="'+dataset[i].pemeriksaan1+'"p2="'+dataset[i].pemeriksaan2+'"  mb1="'+dataset[i].masa_berlaku1+'" mb2="'+dataset[i].masa_berlaku2+'" lmp="'+dataset[i].lama_masa_perizinan+'" class="edit" title="Update Record" data-toggle="modal" data-target="#modalLoginForm"><i class="glyphicon glyphicon-pencil"></i></a><a href="#" ip="'+dataset[i].id_perizinan+'" name="ip" title="Delete Record" data-toggle="tooltip" id="hapus" data-target="#" value="hapus"><span class="glyphicon glyphicon-trash"></span></a></td>';              	html += '</tr>';
                
            	}
                html+= "</table>";
                $('#tabel').html(html);

      $("#tabel").on('click','#hapus',function(e){
      e.preventDefault();
      var id_perizinan = $(this).attr('ip');
	  	$.ajax({
          type : "POST",
          url   : '../backend/rekap-perizinan-gedung.php?act=delete',
          data :{'id_perizinan' : id_perizinan},
          method : 'POST',
          success : function(data){
            
          if(data.status == 1){
            location.reload();

          }
            alert(data.message);
            
          },
          error: function (e) {
                    alert(e.message);
                }
          });
		  });
    }
  })

	  $("#tabel").on('click','.edit',function(){
      alert("masuk edit")
      var id_ttc = $(this).attr('it');
      var nama_perizinan = $(this).attr('np');
	    var status_perizinan = $(this).attr('sp');
      var tahun_pembuatan =  $(this).attr('tp');
      var pemeriksaan1 =  $(this).attr('p1');
	    var pemeriksaan2=  $(this).attr('p2');
      var masa_berlaku1 =  $(this).attr('mb1');
      var masa_berlaku2 =  $(this).attr('mb2');
	    var lama_masa_perizinan=  $(this).attr('lmp');

          $("#id_ttc").val(id_ttc);
          $("#nama_perizinan").val(nama_perizinan);
      	  $("#status_perizinan").val(status_perizinan);
          $("#tahun_pembuatan").val(tahun_pembuatan);
      	  $("#pemeriksaan1").val(pemeriksaan1);
          $("#pemeriksaan2").val(pemeriksaan2);
          $("#masa_berlaku1").val(masa_berlaku1);
      	  $("#masa_berlaku2").val(masa_berlaku2);
          $("#lama_masa_perizinan").val(lama_masa_perizinan);
	  	    $("#update").val("update");


		});

 });


</script>
