<?php
session_start();
require_once "global.config.php";
if (!isset($_SESSION[$auth_name])) {
    header('Location: '.base_url('login.php'));
}
$title = "Index";
$auth = (object) $_SESSION[$auth_name];
require_once "component/header.php";
?>
<!-- 
<div class="row wrapper border-bottom white-bg page-heading" style="margin-bottom:10px;">
    <div class="col-lg-12">
        <h2>Home</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.php">Home</a>
            </li>
            <li class="active">
                <a>Dashboard</a>
            </li>
        </ol>
    </div>

</div> -->

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-content">
                <h3>Dashboard TTC</h3>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-content">
                <h3>Dashboard BM</h3>
            
            </div>
        </div>
    </div>
</div>
<?php require_once "component/assets_js.php";?>
<script src="<?=base_url();?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/export-data.js"></script>

<script type="text/javascript">
    $('document').ready(function () {

    });

</script>
<?php require_once "component/footer.php";?>