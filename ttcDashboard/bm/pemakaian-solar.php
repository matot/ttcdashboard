
<?php
session_start();
require_once "../global.config.php";
if (!isset($_SESSION[$auth_name])) {
    header('Location: ' . base_url('login.php'));
}
$title = "Index";
$auth = (object) $_SESSION[$auth_name];
require_once "../component/header.php";
require_once "../_lib/Connector.db.php";

$db = new Db();
$mysqli = $db->connect('server_db', $db_name);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-content">
                <div>
                    <h3 class="pull-left">Pemakaian Solar</h3>
                    <div class="pull-right">
                        <input type="text" class="tahun" value="<?=date('Y');?>" /><button
                            class="btn_filter_tahun">Filter</button>
                    </div>
                    <div id="container_body" style="clear:both;min-width: 310px; height: 50px; margin: 0 auto">
                        
                            <div class="form-group">
                    <button type="button" class="btn btn-sm btn-info btn_tambah_data" data-toggle="modal" data-target="#modalLoginForm"
                        data-url="<?=base_url('backend/pemakaian-solar.php');?>">Input Data</button>
                    <!-- <button type="button" class="btn btn-sm btn-info btn_tambah_data" data-target="#tableData" id="showData">show / hide data</button> -->
                </div> 
            </div>
           <div id="tabel" style="display : block-inline"></div>
           
<?php require_once "../component/footer.php";?>
<!--MODAL AREA TAMBAH DATA-->
<div class="modal inmodal" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInLeft" style="width:50% ; margin-left:25%" >
            <div class="modal-header">
        <h4 class="modal-title w-100 font-weight-bold">Tambah Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-1">

        <div class="md-form mb-1">
        <form id="formInputPemakaianSolar" method="post"
                    action="<?=base_url('backend/pemakaian-solar.php?act=simpan');?>">
                    <table class="table table-sm">

          <label>Uraian kerja *</label>
          <input type="text" id="uraian_kerja" name="uraian_kerja" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-4">
         
          <label>Periode *</label>
          <input type="date" id="tanggal_pemakaian" name="tanggal_pemakaian" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>Liter *</label>
          <input type="double" id="liter_pemakaian" name="liter_pemakaian" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-4">
         
          <label>Keterangan *</label>
          <input type="text" id="keterangan" name="keterangan" class="form-control validate" required="true">
        </div>

        <div class="md-form mb-5">
          
          <label>backup_time *</label>
          <input type="text" id="backup_time" name="backup_time" class="form-control validate" required="true">
        </div>

      </div>
      </table>
      <div>
        <button type="submit" class="btn btn-default" id="submit" value="submit">submit</button>
        <button type="reset" class="btn btn-default" id="reset">reset</button>
        <button type="submit" class="btn btn-default" id="updated" value="update">update</button>
      </div>
      </form>
    </div>
  </div>
</div>

<?php require_once "../component/assets_js.php";?>
<script src="<?=base_url();?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/export-data.js"></script>

<script type="text/javascript">
    $('document').ready(function () {
        $(".datepicker").datepicker({
            format: "yyyy-mm-dd",
            autoclose: true
        });
        $(".bulan_tahun").datepicker({
            format: "mm-yyyy",
            startView: "months",
            viewMode: 1,
            minViewMode: 1,
            autoclose: true
        });
        $(".tahun").datepicker({
            format: "yyyy",
            startView: "years",
            viewMode: 1,
            minViewMode: 2,
            autoclose: true
        });

    });

        $(document).on('submit', 'form#formInputPemakaianSolar', function (e) {
            e.preventDefault();
            var form = $(this);
            var data = new FormData(form[0]);
            jQuery.ajax({
                url: form.attr('action'),
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST',
                success: function (data) {
                  var dataR = data.result;

                  if (data.status == 1) {
                    location.reload();
                    } 
                    alert(data.message);

                },
                error: function (e) {
                    alert(e.message);
                }
            });
        });


        $(document).ready(function() {
        var aksi = $("#submit").val();
        
		    	$.ajax({
	            type : "POST",
	            url   : '../backend/pemakaian-solar.php?act=show_data',
	            async : false,
              dataType : 'json',
             
	            success : function(data){
                var html = '';

                html = "<table class='table table-bordered table-striped' id='tableData' style='text-align : center;'>";
                html += ` <tr>
                                                <th><center>No</th>
                                                <th><center>URAIAN KERJA</th>
                                                <th><center>PERIODE</th>
                                                <th><center>LITER</th>
                                                <th rowspan="3" style="padding-top : 40px;"><center>KETERANGAN <br>Acoupancy</th>
                                                <th rowspan="3" style="padding-top : 40px;"><center>BACKUP TIME</th>
                                                <th rowspan="3" style="padding-top : 40px;"><center>ACTION</th>
                                                </tr>`;

                var count = 0;
                var hasil = 0;
                var totalLiter = 0;
                var dataResult = data.result;

                for (let i = 0; i < dataResult.length; i++) {
                 count+= dataResult[i].liter_pemakaian++;
                }
                hasil = count / dataResult.length;
                totalLiter = hasil.toString().slice(0,6);

                html += ` <tr style="font-weight : bold">
                                                <td style="padding-top : 25px;" rowspan="2"><center>A</center></td>
                                                <td colspan="2"><center>Kapasitas Tangki</center></td>
                                                <td>${totalLiter}</td>
                                                </tr>

                                                <tr style="font-weight : bold">
                                                <td colspan="2"><center>Stok saat ini (awal bulan)</center></td>
                                                <td>${totalLiter}</td>
                                                </tr>`;
                                                
              var dataset = data.result;
              for(var i=0; i<dataset.length; i++){
              	html += '<td><center>'+(i+1)+'</td>';
              	html += '<td >'+dataset[i].uraian_kerja+'</td>';
                html += '<td >'+dataset[i].tanggal_pemakaian+ '</td>';
                html += '<td >'+(dataset[i].liter_pemakaian-1)+'</td>';
                html += '<td >'+dataset[i].keterangan+ '</td>';
                html += '<td >'+dataset[i].backup_time+ '</td>';
              	html += '<td><a href="#" uk="'+dataset[i].uraian_kerja+'" tp="'+dataset[i].tanggal_pemakaian+'" lp="'+dataset[i].liter_pemakaian+'" ket="'+dataset[i].keterangan+'" bt="'+dataset[i].backup_time+'" class="edit" title="Update Record" data-toggle="modal" data-target="#modalLoginForm"><span class="glyphicon glyphicon-pencil"></span></a><a href="#" uk="'+dataset[i].uraian_kerja+'" class="hapus" title="Delete Record" data-toggle="tooltip" data-target="#" name="hapus"><span class="glyphicon glyphicon-trash"></span></a></td>';
                // html+= '<td><a href="#" ips="'+dataset[i].id_pemakaian_solar+'" id="hapus" name="ips" title="Delete Record" data-toggle="tooltip" data-target="#" value="hapus"><span class="glyphicon glyphicon-trash"></span></a></td>';
              	html += '</tr>';

            	}
                html+=`<tr style="background-color : #D35230; color : white;">
                          <td colspan="3"><center>Total Pemakaian Selama 1 bulan</td>
                          <td>${totalLiter}</td>
                          <td></td>
                          <td></td>
                          <td></td>`;
                html += "</table>";

                $('#tabel').html(html);
             
      $("#tabel").on('click','#hapus',function(e){
          e.preventDefault();         
          var id_pemakaian_solar = $(this).attr('ips');
          console.log(id_pemakaian_solar);
          
	  	$.ajax({
          type : "POST",
          url   : '../backend/pemakaian-solar.php?act=delete',
          data :{'id_pemakaian_solar' : id_pemakaian_solar},
          method : 'POST',
          success : function(data){
          if(data.status == 1){
            location.reload();

          }
            alert(data.message);
            
          },
          error: function (e) {
                    alert(e.message);
                }
            });
		    });
      }
    })              
});
	  $("#tabel").on('click','.edit',function(){
      alert("masuk edit")
      var uraian_kerja = $(this).attr('uk');
	  	transform = uraian_kerja;
	  	var tanggal_pemakaian = $(this).attr('tp');
      var liter_pemakaian =  $(this).attr('lp');
      var keterangan =  $(this).attr('ket');
		  var backup_time=  $(this).attr('bt');
      var act = $("#updated").val();
      var submit = $("#submit").val();

          $("#uraian_kerja").val(uraian_kerja);
      	  $("#tanggal_pemakaian").val(tanggal_pemakaian);
          $("#liter_pemakaian").val(liter_pemakaian - 1);
      	  $("#keterangan").val(keterangan);
          $("#backup_time").val(backup_time);
        
		});
        
</script>

