/*
 Navicat Premium Data Transfer

 Source Server         : 10.47.150.143 Brili
 Source Server Type    : MySQL
 Source Server Version : 50632
 Source Host           : 10.47.150.143:3306
 Source Schema         : inventory_ttc

 Target Server Type    : MySQL
 Target Server Version : 50632
 File Encoding         : 65001

 Date: 23/07/2019 10:30:12
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for adt_pemakaian_energi
-- ----------------------------
DROP TABLE IF EXISTS `adt_pemakaian_energi`;
CREATE TABLE `adt_pemakaian_energi`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_item` int(11) UNSIGNED NULL DEFAULT NULL,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kapasitas` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pemakaian` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `persentase` double(5, 2) NULL DEFAULT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `bulan` char(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun` year NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_thn_bulan`(`id_ttc`, `bulan`, `tahun`, `id_item`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 98 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_pemakaian_energi
-- ----------------------------
INSERT INTO `adt_pemakaian_energi` VALUES (17, 1, 'ttc_bsd', '1', '2', 3.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (18, 2, 'ttc_bsd', '4', '5', 6.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (19, 3, 'ttc_bsd', '7', '8', 9.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (20, 4, 'ttc_bsd', '10', '11', 12.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (21, 5, 'ttc_bsd', '13', '14', 15.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (22, 6, 'ttc_bsd', '16', '17', 18.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (23, 7, 'ttc_bsd', '19', '20', 21.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (24, 8, 'ttc_bsd', '22', '23', 24.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (25, 9, 'ttc_bsd', '25', '26', 27.00, '-', '02', 2019, '2019-07-02 17:48:33');
INSERT INTO `adt_pemakaian_energi` VALUES (26, 1, 'ttc_bsd', '35', '32', 42.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (27, 2, 'ttc_bsd', '34', '65', 23.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (28, 3, 'ttc_bsd', '54', '23', 65.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (29, 4, 'ttc_bsd', '23', '65', 45.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (30, 5, 'ttc_bsd', '34', '34', 23.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (31, 6, 'ttc_bsd', '52', '32', 54.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (32, 7, 'ttc_bsd', '45', '55', 345.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (33, 8, 'ttc_bsd', '23', '45', 54.00, '-', '03', 2019, '2019-07-03 11:49:39');
INSERT INTO `adt_pemakaian_energi` VALUES (34, 1, 'ttc_bsd', '34', '65', 23.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (35, 2, 'ttc_bsd', '54', '65', 34.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (36, 3, 'ttc_bsd', '45', '65', 34.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (37, 4, 'ttc_bsd', '65', '34', 65.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (38, 5, 'ttc_bsd', '54', '23', 34.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (39, 6, 'ttc_bsd', '54', '23', 54.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (40, 7, 'ttc_bsd', '54', '23', 23.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (41, 8, 'ttc_bsd', '54', '23', 54.00, '-', '04', 2019, '2019-07-03 11:51:35');
INSERT INTO `adt_pemakaian_energi` VALUES (42, 1, 'ttc_bsd', '54', '23', 54.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (43, 2, 'ttc_bsd', '54', '23', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (44, 3, 'ttc_bsd', '65', '54', 54.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (45, 4, 'ttc_bsd', '34', '45', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (46, 5, 'ttc_bsd', '34', '65', 65.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (47, 6, 'ttc_bsd', '25', '65', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (48, 7, 'ttc_bsd', '45', '45', 65.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (49, 8, 'ttc_bsd', '45', '65', 34.00, '-', '05', 2019, '2019-07-03 11:52:08');
INSERT INTO `adt_pemakaian_energi` VALUES (50, 1, 'ttc_bsd', '54', '65', 45.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (51, 2, 'ttc_bsd', '54', '34', 65.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (52, 3, 'ttc_bsd', '34', '65', 65.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (53, 4, 'ttc_bsd', '65', '54', 34.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (54, 5, 'ttc_bsd', '34', '65', 34.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (55, 6, 'ttc_bsd', '54', '23', 54.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (56, 7, 'ttc_bsd', '65', '45', 65.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (57, 8, 'ttc_bsd', '34', '54', 34.00, '-', '06', 2019, '2019-07-03 11:52:44');
INSERT INTO `adt_pemakaian_energi` VALUES (58, 1, 'ttc_bsd', '46', '34', 34.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (59, 2, 'ttc_bsd', '32', '34', 34.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (60, 3, 'ttc_bsd', '34', '43', 54.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (61, 4, 'ttc_bsd', '23', '54', 23.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (62, 5, 'ttc_bsd', '23', '54', 54.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (63, 6, 'ttc_bsd', '23', '54', 23.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (64, 7, 'ttc_bsd', '54', '54', 23.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (65, 8, 'ttc_bsd', '22', '35', 54.00, '-', '07', 2019, '2019-07-03 11:53:26');
INSERT INTO `adt_pemakaian_energi` VALUES (82, 1, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (83, 2, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (84, 3, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (85, 4, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (86, 5, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (87, 6, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (88, 7, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (89, 8, 'ttc_bsd', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:32:30');
INSERT INTO `adt_pemakaian_energi` VALUES (90, 9, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:22');
INSERT INTO `adt_pemakaian_energi` VALUES (91, 10, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:23');
INSERT INTO `adt_pemakaian_energi` VALUES (92, 11, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:23');
INSERT INTO `adt_pemakaian_energi` VALUES (93, 12, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:23');
INSERT INTO `adt_pemakaian_energi` VALUES (94, 13, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:23');
INSERT INTO `adt_pemakaian_energi` VALUES (95, 14, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:23');
INSERT INTO `adt_pemakaian_energi` VALUES (96, 15, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:23');
INSERT INTO `adt_pemakaian_energi` VALUES (97, 16, 'ttc_tbs', '12', '12', 12.00, '-', '01', 2019, '2019-07-03 12:37:23');

SET FOREIGN_KEY_CHECKS = 1;
