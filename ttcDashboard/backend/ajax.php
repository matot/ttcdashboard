<?php
    session_start();
    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
    require_once("_lib/Connector.db.php");
    $db = new Db();
    require_once("_lib/Helper.php");
    $helper = new Helper();

    if(!isset($_SESSION['auth_jawaraqr'])){
        $helper->result_error(null,"Please login again.");
    }
    $mysqli = $db->connect('server_db','JawaraQR');
    date_default_timezone_set('Asia/Jakarta');
    // mb_language('uni'); 
    // mb_internal_encoding('UTF-8');
	// $mysqli->query("set names 'utf8'");


    
    function sanitate($val) {
        global $mysqli;
        return $mysqli->real_escape_string($val);
    }
    function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }
    $act = (isset($_GET['act'])) ? $_GET['act'] : null;
    $user = $_SESSION['auth_jawaraqr'];
    switch ($act) {
        case 'participant_list':
            if(!isset($_POST['event'])){
                echo json_encode(array('data'=>[]));
                exit();
            }
            $id_event=sanitate($_POST['event']);
            $query = "SELECT * FROM tbl_participant WHERE id_event='$id_event' ORDER by clock_in_time DESC, `name` ASC";
            $query_participant_list = $mysqli->query($query);
            $result = [];
            $no = 1;
            while ($row = $query_participant_list->fetch_object()) {
                $result[] = array($no,$row->name,$row->pin, $row->clock_in_time); 
                $no++;
            }
            echo json_encode(array('data'=>$result));
            $mysqli->close();
        break;


        case 'event_list':
            $select = "id_event, event_name, event_location, event_end, event_start, created_by, is_deleted, is_lucky_draw,created_at";
            if($user->privilege=="admin"){
                $query = "SELECT $select FROM tbl_event WHERE is_deleted=0 ORDER by created_at DESC";
            }else {
                $query = "SELECT $select FROM tbl_event WHERE is_deleted=0 AND created_by='$user->username' ORDER by created_at DESC";
            }
            $query_event_list = $mysqli->query($query);
            $result = [];
            $no = 1;
            while ($row = $query_event_list->fetch_object()) {
                $btn = "";
                if($row->is_lucky_draw==1){
                    $btn .= "<a href='lucky_draw.php?event=".$row->id_event."' target='_blank' class='btn btn-success btn-sm'>Lucky Draw</a>";
                    $btn .= "<a href='lucky_draw2.php?event=".$row->id_event."' target='_blank' class='btn btn-success btn-sm'>Lucky Draw V2</a>";
                }
                    $btn .= "<button data-id='$row->id_event'data-toggle='modal' data-target='#luckyDrawModal' class='btn btn-sm btn-info open_lucky_draw'>Re/Setup Lucky Draw</button> ";
                
                    $btn .= "<a href='../index.html?event=".$row->id_event."&auth=".base64_encode($user->username)."&time=".base64_encode(date('Y-m-d H:i:s'))."' target='_blank' class='btn btn-primary btn-sm'>Add Participant Non-WL</a>";
                $btn .= "<button data-id='$row->id_event'data-toggle='modal' data-target='#participantListModal' class='btn btn-sm btn-warning open_list_participant'>List Participant</button> ";
                $btn .= "<button data-id='$row->id_event' class='btn btn-sm btn-danger delete_event'>Delete</button>";
                $result[] = array($no,$row->event_name,$row->event_location, $row->event_start, $row->event_end, $row->created_at,$btn); 
                $no++;
            }
            echo json_encode(array('data'=>$result));
            $mysqli->close();
        break;
        case 'lucky_draw_participant': 
            if(!isset($_POST['event'])){
                echo json_encode(null);
                exit();
            }
            $id_event = sanitate($_POST['event']);
            $query = $mysqli->query("SELECT id, CONVERT(`name` USING utf8) as `name`,CONVERT(pin USING utf8) as pin FROM tbl_participant WHERE id_event='$id_event' AND is_winner=0 AND clock_in_time IS NOT NULL");

            if($query->num_rows>0){
                $res = array();
                while ($row = $query->fetch_assoc()) {
                    // echo $row['nama_tim'];
                    // echo "<br/>";
                    $res[] = $row;
                }
                echo json_encode($res,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
            }else{
                echo json_encode(null);
            }
            $mysqli->close();
        break;
        case 'lucky_draw_prize_list_all':
            if(!isset($_POST['event'])){
                echo json_encode(null);
                exit();
            }
            $id_event = sanitate($_POST['event']);
            $query = $mysqli->query("SELECT prize_name,quota FROM tbl_lucky_prize WHERE id_event='$id_event' AND quota > 0 ORDER BY id DESC");
            $res = array();
            if($query->num_rows>0){
                $res = array();
                while ($row = $query->fetch_assoc()) {
                    // echo $row['nama_tim'];
                    // echo "<br/>";
                    $res[] = $row;
                }
                echo json_encode($res);
            }else{
                echo json_encode(null);
            }
            $mysqli->close();
        break;
        case 'lucky_draw_prize_list':
            if(!isset($_POST['event'])){
                echo json_encode(null);
                exit();
            }
            $id_event = sanitate($_POST['event']);
            $query = $mysqli->query("SELECT * FROM tbl_lucky_prize WHERE id_event='$id_event' AND quota > 0 ORDER BY id ASC LIMIT 1");
            $res = array();
            if($query->num_rows>0){
                echo json_encode($query->fetch_assoc());
            }else{
                echo json_encode(null);
            }
            $mysqli->close();
        break;
        case 'lucky_draw_winner':
            if(!isset($_POST['event'])){
                echo json_encode(array('data'=>[]));
                exit();
            }
            $id_event = sanitate($_POST['event']);
            $query = "SELECT * FROM tbl_lucky_winner WHERE id_event='$id_event' ORDER BY id DESC";
            $list = $mysqli->query($query);
            $res = array();
            if($list->num_rows >0){
                while ($row = $list->fetch_object()) {
                    $res[$row->prize_name][] = $row;
                }
                echo json_encode($res);
            }else {
                echo json_encode(null);
            }
            
            
            $mysqli->close();
        break;
        case 'lucky_draw_add_winner':
            if(!isset($_POST['event'])){
                echo json_encode(null);
                exit();
            }
            $id_event = sanitate($_POST['event']);
            $id_participant = sanitate($_POST['row']['id']);
            $name = sanitate($_POST['row']['name']);
            $pin = sanitate($_POST['row']['pin']);
            $id_prize = sanitate($_POST['prize']['id']);
            $prize_name = sanitate($_POST['prize']['prize_name']);
            
            
            $res = $mysqli->query("INSERT INTO tbl_lucky_winner (id_participant,`name`,pin,id_prize,prize_name,id_event) VALUES ('$id_participant','$name','$pin','$id_prize','$prize_name','$id_event')");
            
            if($res){
                $date = date("Y-m-d H:i:s");
                $mysqli->query("UPDATE tbl_participant SET is_winner='1', winner_at='$date' WHERE id='$id_participant'");
                $mysqli->query("UPDATE tbl_lucky_prize SET quota=quota-1 WHERE id='$id_prize'");
                echo json_encode(array('message'=>'Congratulations','res'=>$res),JSON_PRETTY_PRINT);
            }else {
                echo json_encode(array('message'=>'Failed to save','res'=>$res),JSON_PRETTY_PRINT);
            }
            $mysqli->close();
        break;
        case 'lucky_draw_remove_winner':
            if(!isset($_POST['event'])){
                echo json_encode(null);
                exit();
            }
            $id_event = sanitate($_POST['event']);
            $id = sanitate($_POST['id']);
            $check = $mysqli->query("SELECT * FROM tbl_lucky_winner WHERE id='$id' AND id_event='$id_event'");
            if($check->num_rows==1){
                $row = (object) $check->fetch_assoc();
                $query = $mysqli->query("DELETE FROM tbl_lucky_winner WHERE id='$id'");
                $mysqli->query("UPDATE tbl_participant SET is_winner='0', winner_at=NULL WHERE id='$row->id_participant' AND id_event='$id_event'");
                if($query){
                    $mysqli->query("UPDATE tbl_lucky_prize SET quota=quota+1 WHERE id='$row->id_prize'");
                    $res = array("message"=>"Delete Success",'res'=>$query);
                    
                }else {
                    $res = array("message"=>"Delete Failed",'res'=>$query);
                }
            }else {
                $res = array("message"=>"Data Not Found",'res'=>false);
            }
            echo json_encode($res);
            $mysqli->close();
        break;
        case 'lucky_draw_setup':
            header('Content-Type: text/html;charset=utf-8');  
            if(!isset($_POST['event'])){
                echo json_encode(null);
                exit();
            }
            $id_event = sanitate($_POST['event']);

            $prize = ($_POST['prize']);
            $quota = ($_POST['quota']);
            $is_lucky_draw = sanitate(@$_POST['is_lucky_draw']);
            $is_lucky_draw = ($is_lucky_draw) ? $is_lucky_draw : 0;
            $check = $mysqli->query("SELECT id_event FROM tbl_event WHERE id_event='$id_event'");
            if($check->num_rows==1){
                $row = (object) $check->fetch_assoc();
                if(!empty($_FILES['wallpaper_lucky_draw']['tmp_name'])){
                    $image = addslashes(file_get_contents($_FILES['wallpaper_lucky_draw']['tmp_name']));
                }else {
                    $image = null;
                }
                //var_dump($_POST);
                $update_event = $mysqli->query("UPDATE tbl_event SET wallpaper_lucky_draw='$image', is_lucky_draw='$is_lucky_draw' WHERE id_event='$id_event'");
                $mysqli->query("DELETE FROM tbl_lucky_prize WHERE id_event='$id_event'");
                $mysqli->query("DELETE FROM tbl_lucky_winner WHERE id_event='$id_event'");
                $mysqli->query("UPDATE tbl_participant SET is_winner=0, winner_at=NULL WHERE id_event='$id_event' AND is_winner=1");
                if($update_event && $is_lucky_draw==1){
                    if(count($prize) == count($quota)){
                        
                        for($x=count($prize)-1;$x>=0;$x--){
                            $mysqli->query("INSERT INTO tbl_lucky_prize (id_event,prize_name,quota) VALUES ('$id_event','$prize[$x]','$quota[$x]')");
                        }
                    }
                    echo '
                        <script>alert("Lucky Draw Activated"); window.location.href="admin.php";</script>
                    ';
                }else if($update_event){
                    
                    echo '
                        <script>alert("Lucky Draw is Deactive"); window.location.href="admin.php";</script>
                    ';
                }else {
                    echo '
                        <script>alert("'.$mysqli->error.'"); window.location.href="admin.php";</script>
                    ';
                    
                }
                
            }else {
                echo '
                    <script>alert("Event Not Found"); window.location.href="admin.php";</script>
                ';
            }
            $mysqli->close();
        break;
        case 'lucky_draw_export':
            header('Content-Type: text/html;charset=utf-8');  
            $id_event = $_GET['event'];
            $q = $mysqli->query("SELECT * FROM tbl_lucky_winner where id_event='$id_event'");
            echo "<style>
                    table {border:1px;min-width:60%;margin:0 auto;width:80%;border-collapse: collapse;}
                    th {text-align:left;}
                    th,td {border:1px solid #555;padding:4px;}
                    @media print {
                        table {
                            width:90%;
                        }
                    }
                </style>
                <table border='1'>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Pin</th>
                        <th>Prize</th>
                        <th>Winner at</th>
                    </tr>";
            $no = 1;
            while($row = $q->fetch_object()){
                echo "<tr>
                    <td>$no</td>
                    <td>$row->name</td>
                    <td>$row->pin</td>
                    <td>$row->prize_name</td>
                    <td>$row->winner_at</td>
                </tr>";
                $no++;
            }
            echo "</table>";
            $mysqli->close();
        break;
        case 'submit_event':
            $event_name = isset($_POST['event_name']) ? sanitate($_POST['event_name']) : null;
            $event_location = isset($_POST['event_location']) ? sanitate($_POST['event_location']) : null;
            $event_start = isset($_POST['event_start']) ? sanitate($_POST['event_start']) : null;
            $event_end = isset($_POST['event_end']) ? sanitate($_POST['event_end']) : null;
            $file_participant = isset($_FILES['file_participant']) ? $_FILES['file_participant'] : null;
            if(empty($event_name) || empty($event_location) || empty($event_start) || empty($event_end) || empty($file_participant['name'])){
                $helper->result_error(null,"Please fill required field");
            }
            $file = $file_participant['tmp_name'];
            //$isUploaded = copy($file_participant['tmp_name'], $file);
            if($file) {
                /** PHPExcel_IOFactory */
                require_once ('PHPExcel/Classes/PHPExcel/IOFactory.php');
                require_once ('PHPExcel/Classes/PHPExcel.php');

                $callStartTime = microtime(true);
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($file);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($file);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
                    //$this->session->set_flashdata('msg_flash', 'Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME));
                    //redirect('dashboard');
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $result = array();
                if($highestColumn != "B"){
                    $helper->result_error(null,"Excel format is incorrect, excel column should be from A to C");
                }
                $id_event = substr(md5(microtime()),rand(0,26),6);
                $saved_count = 0;
                for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                    NULL,
                                                    TRUE,
                                                    FALSE);
                    $name = (string) sanitate($rowData[0][0]);
                    $pin = (string)  sanitate($rowData[0][1]);
                    $id = substr(md5(microtime()),rand(0,26),6);
                    $q_save = $mysqli->query("INSERT INTO tbl_participant (id,`name`,pin,id_event) VALUES ('$id','$name','$pin','$id_event')");
                    if($q_save){
                        $saved_count++;
                    }
                }
                $rows_saved_must_be = $highestRow-1;
                if($saved_count==$rows_saved_must_be){
                    $query_save = $mysqli->query("INSERT INTO tbl_event (id_event,event_name,event_location,event_start,event_end,created_by) VALUES ('$id_event','$event_name','$event_location','$event_start','$event_end','$user->username')");
                    if($query_save){
                        $user_info = $helper->user_info();
                        $mysqli->query("INSERT INTO tbl_user_log (action_name,username,agent,ip_address) VALUES ('create_new_event','$user->username','$user_info->agent','$user_info->ip')");
                        $helper->result($query_save,"Successfully create event '$event_name'");
                    }
                }else {
                    $helper->result_error(array("rows"=>$rows_saved_must_be,"saved_rows"=>$saved_count),"The number of lines in the excel file does not match the saved $rows_saved_must_be vs $saved_count");
                }
                
            }else {
                $helper->result_error($isUploaded,"Error Upload $file");
            }
            
            $mysqli->close();
        break;


        case 'delete_event':
            $rawData = file_get_contents("php://input");
            $req = json_decode($rawData);
            if(!$req){
                $req = (object) $_POST;
            }
            //var_dump($req);
            $event = (isset($req->event)) ? sanitate($req->event) : null;
            if(!$event){
                $helper->result_error($event,"Please send me event id");
            }
            if($user->privilege=="admin"){
                $query = "SELECT id_event FROM tbl_event WHERE is_deleted=0 AND id_event='$event'";
            }else {
                $query = "SELECT id_event FROM tbl_event WHERE is_deleted=0 AND id_event='$event' AND created_by='$user->username'";
            }
            $query_event = $mysqli->query($query);
            if($query_event->num_rows > 0){
                $deleted = $mysqli->query("UPDATE tbl_event set is_deleted=1 WHERE id_event='$event'");
                $user_info = $helper->user_info();
                $mysqli->query("INSERT INTO tbl_user_log (action_name,username,agent,ip_address) VALUES ('deleted_event_$event','$user->username','$user_info->agent','$user_info->ip')");
                $helper->result($deleted,"Successfully deleted event");
            }else {
                $helper->result_error(null,"Event Not Found");
            }
            $mysqli->close();
        break;
        case 'check_qr':
            $rawData = file_get_contents("php://input");
            $req = json_decode($rawData);
            if(!$req){
                $req = (object) $_POST;
            }
            //var_dump($req);
            $qr = (isset($req->qr)) ? sanitate($req->qr) : null;
            if(!$qr){
                $helper->result_error($qr,"Mohon Maaf, Kode QR harus dikirim");
            }
            $base64 = base64_decode($qr);
            $decoded = explode("|",$base64,3);
            //var_dump($decoded);
            if(@$decoded[0] ==null || @$decoded[2] == null){
                $helper->result_error($qr,"Mohon Maaf, kode QR salah.");
            }
            $id_event = $decoded[1];
            $pin = $decoded[0];
            

            $query = "SELECT id,id_event,`name`,clock_in_time FROM tbl_participant WHERE pin='$pin' AND id_event='$id_event'";
            
            $participant = $mysqli->query($query);
            if($participant->num_rows==1){
                $datetime = date("Y-m-d H:i:s");
                $row = $participant->fetch_object();
                $id = $row->id;
                if($row->clock_in_time == null){
                    $row->clock_in_time = $datetime;
                    $updated = $mysqli->query("UPDATE tbl_participant set clock_in_time='$datetime' WHERE id='$id'");
                    $helper->result($row,"Selamat Datang ".$row->name);
                }else {
                    $helper->result_error(null,"Anda sudah terdaftar hadir sebagai ".$row->name);
                }
                
            }else {
                $helper->result_error(null,"Participant Not Found");
            }
            $mysqli->close();
        break;
        case 'save_photo':
            $rawData = file_get_contents("php://input");
            $req = json_decode($rawData);
            if(!$req){
                $req = (object) $_POST;
            }
            //var_dump($req);
            $imgBase64 = (isset($req->imgBase64)) ? $req->imgBase64 : null;
            $id = (isset($req->id)) ? $req->id : null;
            $id_event = (isset($req->id_event)) ? $req->id_event : null;
            if(!$imgBase64 || !$id_event || !$id){
                $helper->result_error(null,"Mohon Maaf, data tidak boleh kosong");
            }
            $path = 'photo/'.$id_event;
            if (!file_exists($path)) {
                mkdir($path, 0777, true);
            }
            $fileData = base64_decode($imgBase64);
            //saving
            $fileName = $path.'/'.$id.'.png';
            $save = file_put_contents($fileName, $fileData);
            $helper->result($save,"File Saved");
            $mysqli->close();
        break;

        case 'create_user_admin':
            if(isset($_POST['username']) && isset($_POST['password']) && isset($_POST['password2']) && ($user->privilege=="admin")){
                $username = sanitate($_POST['username']);
                $password = sanitate($_POST['password']);
                $password2 = sanitate($_POST['password2']);
                if($password==$password2){
                    $password_hash = password_hash($password, PASSWORD_DEFAULT);
                    $query_save_user = $mysqli->query("INSERT INTO tbl_user (username,`password`,privilege) VALUES ('$username','$password_hash','user')");
                    if($query_save_user){
                        $helper->result($query_save_user,"User Created");
                    }else {
                        $helper->result($query_save_user,"Failed create user, try with different username",0);
                    }
                }else {
                    $helper->result(null,"Password not match",0);
                }
            }else {
                $helper->result_error(null,"Unknown Request");
            }
            $mysqli->close();
        break;
        default:
            # code...
        break;
    }
