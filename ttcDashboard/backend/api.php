<?php
    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
    header("Access-Control-Allow-Origin: *");
    date_default_timezone_set('Asia/Jakarta');
    require_once("_lib/Connector.db.php");
    $db = new Db();
    require_once("_lib/Helper.php");
    $helper = new Helper();
    $apikey = "s09n42g9ina92";

    $rawData = file_get_contents("php://input");
    $req = json_decode($rawData);
    if(!$req){
        $req = (object) $_POST;
    }
    

    $mysqli = $db->connect('server_db','JawaraQR');
    function sanitate($val) {
        global $mysqli;
        return $mysqli->real_escape_string($val);
    }
    
    $act = (isset($_GET['act'])) ? $_GET['act'] : null;
    switch ($act) {
        case 'event_list':

            if(!isset($req->apikey) || (isset($req->apikey) && $req->apikey != $apikey)){
                $helper->result_error("Your access is restricted");
            }
            $query = "SELECT id_event,event_name FROM tbl_event WHERE is_deleted='0' AND event_end >= CURDATE() ORDER by event_start DESC, event_name ASC";
            $query_event_list = $mysqli->query($query);
            $result = [];
            $no = 1;
            while ($row = $query_event_list->fetch_object()) {
                $result[] = $row; 
                $no++;
            }
            $helper->result($result,"Event List");
        break;
        case 'check_event_participant':
            
            if(!isset($req->apikey) || (isset($req->apikey) && $req->apikey != $apikey)){
                $helper->result_error("Your access is restricted");
            }
            if(!$req->event_id || !$req->pin){
                $helper->result_error("Please send event id and pin");
            }
            $id_event = sanitate($req->event_id);
            $pin = sanitate($req->pin);
            $user_info = $helper->user_info();
            
            $query = $mysqli->query("SELECT id,`name` FROM tbl_participant WHERE pin='$pin' AND id_event='$id_event'");
            if($query->num_rows>0){
                $row = $query->fetch_object();
                $encoded = base64_encode("$pin|$id_event|".date("H:i:s"));
                $mysqli->query("INSERT INTO tbl_generated_log (agent,ip_address,pin,id_event,generated_qr) VALUES ('$user_info->agent','$user_info->ip','$pin','$id_event','$encoded')");
                $helper->result($encoded,"Participant Verified");
            }else {
                $q = $mysqli->query("INSERT INTO tbl_generated_log (agent,ip_address,pin,id_event) VALUES ('$user_info->agent','$user_info->ip','$pin','$id_event')");
                //var_dump($q);
                $helper->result_null($query->num_rows,"Participant Not Found");
            }
            
        break;
            case 'check_event':
                
            if(!isset($req->apikey) || (isset($req->apikey) && $req->apikey != $apikey)){
                $helper->result_error("Your access is restricted");
            }
            if(!$req->id_event){
                $helper->result_error("Please send event id");
            }
            $id_event = sanitate($req->id_event);
            
            $query = $mysqli->query("SELECT id_event,event_name,event_start,event_end,is_wl_participant FROM tbl_event WHERE is_deleted=0 AND id_event='$id_event'");
            if($query->num_rows>0){
                $row = $query->fetch_object();
               
                $helper->result($row,"Event Verified");
            }else {
                $helper->result($query->num_rows,"Event Not Found",0);
            }
            
        break;
        break;
            case 'add_participant':
                
            if(!isset($req->apikey) || (isset($req->apikey) && $req->apikey != $apikey)){
                $helper->result_error("Your access is restricted");
            }
            if(!$req->id_event){
                $helper->result_error("Please send event id");
            }
            if(!$req->name || !$req->phone_number || !$req->auth){
                $helper->result_error("Please send required field.");
            }
            $id_event = sanitate($req->id_event);
            $name = sanitate($req->name);
            $pin = sanitate($req->phone_number);
            $username = base64_decode(sanitate($req->auth));
            $clockin_now = sanitate($req->clockin_now);
            if($clockin_now==1){
                $clockin = "'".date("Y-m-d H:i:s")."'";
                $message = "Welcome $name : $clockin";
            }else {
                $clockin = 'NULL';
                $message = "Add $name as participant";
            }
            $id = substr(md5(microtime()),rand(0,26),6);
            $query = $mysqli->query("SELECT pin FROM tbl_participant WHERE pin='$pin' AND id_event='$id_event'");
            if($query->num_rows<1){
                $insert = $mysqli->query("INSERT INTO tbl_participant (id,`name`,pin,id_event,clock_in_time) VALUES ('$id','$name','$pin','$id_event',$clockin)");
                if($insert){
                    $helper->result($insert,"$message");
                }else {
                    $helper->result(null,$mysqli->error,0);
                }
                
            }else {
                $helper->result(null,"$name has been registered as a participant",0);
            }
            
        break;
        default:
            $helper->result_error("Your access is restricted");
        break;
    }

    $mysqli->close();