<?php 
    session_start();
    if(!isset($_SESSION['auth_jawaraqr'])){
        header('Location: login.php');
    }
    $user = $_SESSION['auth_jawaraqr'];
    
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JawaraQR - Admin</title>
    <link rel="shortcut icon" href="favicon.png" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <style>
        .mt-0 {
            margin-top: 0 !important;
        }

        .small-td tbody>tr>td,
        .small-td>tfoot>tr>td {
            padding: 3px;
        }

        div.dataTables_wrapper div.dataTables_filter {
            float: right;
            text-align: right;
        }
    </style>
</head>

<body class="top-navigation">

    <div id="wrapper">
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom white-bg">
                <nav class="navbar navbar-static-top" role="navigation">
                    <div class="navbar-header">
                        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar"
                            data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                            <i class="fa fa-reorder"></i>
                        </button>
                        <a href="#" class="navbar-brand">JawaraQR Admin</a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbar">
                        <ul class="nav navbar-nav">
                            <li class="active">
                                <a aria-expanded="false" role="button" href="index.php"> Back to QR Reader</a>
                            </li>
                            <?php if($user->privilege =="admin"){?>
                            <li>
                                <a aria-expanded="false" role="button" href="#" data-toggle="modal"
                                    data-target="#createUserAdminModal"> Create Admin</a>
                            </li>
                            <?php } ?>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <a href="login.php?act=logout">
                                    <i class="fa fa-sign-out"></i> Log out
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="wrapper wrapper-content">
                <div class="container">

                    <div class="row">
                        <div class="col-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">

                                    <button data-toggle="modal" data-target="#createEventModal"
                                        class="btn btn-sm btn-primary pull-right"><i class="fa fa-plus"></i>
                                        Create New Event</button>
                                    <h3>Event (<?php echo $user->username;?>)</h3>
                                </div>
                                <div class="ibox-content">
                                    <div class="table-responsive">
                                        <table id="event_table"
                                            class="table table-striped table-bordered table-hover dataTables-example"
                                            style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th width="40px">No.</th>
                                                    <th>Event Name</th>
                                                    <th>Location</th>
                                                    <th>Start</th>
                                                    <th>End</th>
                                                    <th>Created At</th>
                                                    <th class="center" width="150px">Action</th>
                                                </tr>
                                            </thead>

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="overfly row">
                        <div id="output" class="col-lg-6 col-md-8 col-12 align-self-center animated bounce"></div>
                    </div>



                </div>

            </div>

            <div class="footer">
                <div class="pull-right">
                    A2 JAWARA
                </div>
                <div>
                    <strong>Copyright</strong> IT Support Business &copy; 2019
                </div>
            </div>

        </div>
    </div>

    <!--MODAL CREATE EVENT-->
    <div class="modal inmodal" id="createEventModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title">Create New Event</h3>
                </div>
                <div class="modal-body">

                    <form id="wizard" method="post" enctype="multipart/form-data">
                        <h1>Event Name</h1>
                        <div class="step-content">
                            <div class="text-center">

                                <div class="form-group">
                                    <label>
                                        <h2 class="mt-0">Event Name *</h2>
                                    </label>
                                    <input id="event_name" name="event_name" type="text" class="form-control required"
                                        placeholder="Gathering Family" required="true">
                                </div>
                            </div>
                        </div>

                        <h1>Location & Date</h1>
                        <div class="step-content">
                            <div class="text-center ">
                                <div class="form-group">
                                    <label>
                                        <h2 class="mt-0">Event Location *</h2>
                                    </label>
                                    <input id="event_location" name="event_location" type="text"
                                        class="form-control required" placeholder="Hotel Family" required="true">
                                </div>

                                <div class="form-group" id="data_5">
                                    <label>
                                        <h2 class="mt-0">Event Date *</h2>
                                    </label>
                                    <div class="input-daterange input-group" id="datepicker" style="width: 100%">
                                        <input type="text" class="input-sm form-control required date"
                                            name="event_start" value="<?=date('Y-m-d');?>" required="true" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" class="input-sm form-control required date" name="event_end"
                                            value="" required="true" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h1>Participants</h1>
                        <div class="step-content">
                            <div class="text-center ">
                                <div class="form-group">
                                    <label>
                                        <h2 class="mt-0">Excel File Participants *</h2>
                                    </label>
                                    <div>
                                        <img src="assets/img/example_format_excel_jawaraQR.png" style="float:left;" />
                                        <div style="padding: 20px;">
                                            The excel column only has participant Name and PIN (A-B) <br />
                                            Download template format <a href="template_jawaraQR.xlsx">here</a>
                                        </div>
                                    </div>
                                    <div class="custom-file">
                                        <input name="file_participant" id="file_participant" type="file"
                                            class="form-control btn btn-primary required">
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>

            </div>
        </div>
    </div>


    <!--MODAL PARTICIPANT LIST-->
    <div class="modal inmodal" id="participantListModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title event_name">Participant List</h3>
                </div>
                <div class="modal-body">
                    <table id="participant_table"
                        class="table table-striped table-bordered table-hover dataTables-example small-td"
                        style="width:100%">
                        <thead>
                            <tr>
                                <th width="40px">No.</th>
                                <th>Name</th>
                                <th>Pin</th>
                                <th>Clock in</th>
                            </tr>
                        </thead>
                    </table>
                </div>

            </div>
        </div>
    </div>


    <!--MODAL Lucky Draw-->
    <div class="modal inmodal" id="luckyDrawModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content animated bounceInLeft">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title">Lucky Draw</h3>
                </div>
                <div class="modal-body">
                    <div class="row" style="text-align:left;">
                    <form method="post" action="ajax.php?act=lucky_draw_setup" enctype="multipart/form-data" onsubmit="return confirm('Do you really want to submit the form? , it will reset the winner and prize');">
                        <input type="hidden" name="event" class="id_event" value=""/>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><input type="checkbox" value="1" id="is_lucky_draw" name="is_lucky_draw" checked /> Activate
                                    Lucky Draw </label>
                            </div>
                            <div class="form-group custom-file lucky_draw_container">
                                <label>Wallpaper Lucky Draw</label>
                                <input name="wallpaper_lucky_draw" id="wallpaper_lucky_draw" type="file"
                                    class="form-control btn">
                            </div>
                        </div>
                        <div class=" col-sm-6 lucky_draw_container">
                            <div class="form-group">
                                <label>Prize</label>
                                <div id="dynamic_form">
                                    <div class="row">
                                    <input type="text" class="col-sm-8" name="prize[0]" placeholder="Grand Prize" />
                                    <input type="number" class="col-sm-3" name="quota[0]" placeholder="Quota" />
                                    <button class="btn_add_prize" onclick="addItem(); return false;">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                                <input type="submit" class="btn btn-block btn-primary" value="Setup Lucky Draw">
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <?php if($user->privilege=="admin") { ?>
    <!--MODAL Create User Admin-->
    <div class="modal inmodal" id="createUserAdminModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInLeft">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title event_name">Create User Admin</h3>
                </div>
                <div class="modal-body">
                    <form id="formCreateUserAdmin" method="post" action="ajax.php?act=create_user_admin">
                        <div class="form-group">
                            <label>Username *</label>
                            <input id="username" name="username" type="text" class="form-control required"
                                placeholder="Username" required="true">
                        </div>
                        <div class="form-group">
                            <label>Password *</label>
                            <input id="password" name="password" type="password" class="form-control required"
                                placeholder="*******" required="true">
                        </div>
                        <div class="form-group">
                            <label>Confirm Password *</label>
                            <input id="password2" name="password2" type="password" class="form-control required"
                                placeholder="*******" required="true">
                        </div>
                        <div class="form-group">
                            <button id="createUserAdminButton" type="submit" class="btn btn-primary">Create User
                                Admin</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <?php } ?>

    <!-- Mainly scripts -->
    <script src="assets/js/jquery-2.1.1.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="assets/js/inspinia.js"></script>
    <script src="assets/js/plugins/pace/pace.min.js"></script>
    <script src="assets/js/plugins/dataTables/datatables.min.js"></script>
    <!-- Steps -->
    <script src="assets/js/plugins/staps/jquery.steps.min.js"></script>
    <!-- Jquery Validate -->
    <script src="assets/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
    var i = 1;
            function addItem() {
//                menentukan target append
                var itemlist = document.getElementById('dynamic_form');
//                membuat element
                var row = document.createElement('div');
                console.log(row);
                row.setAttribute('class','row');
                var prize = document.createElement('input');
                prize.setAttribute('name', 'prize[' + i + ']');
                prize.setAttribute('class', 'col-sm-8');
                prize.setAttribute('placeholder', 'Prize '+(i+1));
                
                var quota = document.createElement('input');
                quota.setAttribute('name', 'quota[' + i + ']');
                quota.setAttribute('class', 'col-sm-3');
                quota.setAttribute('placeholder', 'Quota');
                
//                meng append element
                itemlist.appendChild(row);
                row.appendChild(prize);
                row.appendChild(quota);
//                membuat element input
                
                var hapus = document.createElement('span');
//                meng append element input
                row.appendChild(hapus);
                hapus.innerHTML = '<button class="btn_remove_prize">x</button>';
//                membuat aksi delete element
                hapus.onclick = function () {
                    row.parentNode.removeChild(row);
                    i--;
                };
                i++;
            }

        $(document).ready(function () {

            $("#wizard").steps({
                onStepChanging: function (event, currentIndex, newIndex) {
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex) {
                        return true;
                    }
                    
                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex) {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onFinishing: function (event, currentIndex) {
                    var form = $(this);

                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    var form = $(this);
                    var data = new FormData(form[0]);
                    jQuery.each(jQuery('#file_participant')[0].files, function (i, file) {
                        data.append('file[]', file);
                    });
                    //$.post('ajax.php?act=submit_event', form.serialize())
                    jQuery.ajax({
                        url: 'ajax.php?act=submit_event',
                        data: data,
                        cache: false,
                        contentType: false,
                        processData: false,
                        method: 'POST',
                        type: 'POST', // For jQuery < 1.9
                        success: function (data) {
                            alert(data.message);
                            location.reload();
                        },
                        error: function (e) {
                            alert(e.message);
                        }
                    });
                }
            });
            var event_table = $('#event_table').DataTable({
                "ajax": 'ajax.php?act=event_list',
                "order": [
                    [5, "desc"]
                ],
                dom: 'Bfrtip',
                buttons: [{
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    },
                    {
                        extend: 'pdfHtml5',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5]
                        }
                    }
                ]
            });
            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                format: 'yyyy-mm-dd',
            });
            //     var participant_table = $('#participant_table').DataTable({"ajax": {
            //     "url": "scripts/post.php",
            //     "type": "POST"
            // }});
            $(document).on("click", ".open_list_participant", function () {
                //participant_table.ajax({url:'ajax.php?act=participant_list',data:'a=b',type:'POST'}).load();
                $('#participant_table').DataTable().destroy();
                $('#participant_table td').html("");
                $('#participant_table').DataTable({
                    "ajax": {
                        "url": "ajax.php?act=participant_list",
                        "type": "POST",
                        "data": {
                            event: $(this).data('id')
                        }
                    },
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: [0, 1, 2, 3]
                            }
                        }
                    ]
                });
                //console.log($(this).data('id'));
            });
            $(document).on("click", ".delete_event", function () {
                if (!confirm('Are you sure to delete this event?')) return false;
                $.post("ajax.php?act=delete_event", {
                        event: $(this).data('id')
                    }).done(function (data) {
                        alert(data.message);
                        event_table.ajax.reload();
                    })
                    .fail(function (e) {
                        alert(e.responseJSON.message);
                        location.reload();
                    });

            });

            $(document).on("click", ".open_lucky_draw", function () {
                $(".id_event").val("");
                $(".id_event").val($(this).data('id'));
            });


        });
    </script>
    <?php if($user->privilege=="admin") { ?>
    <script>
        $(document).on("click", "#createUserAdminButton", function () {
            $("#formCreateUserAdmin").validate({
                rules: {
                    password: "required",
                    password2: {
                        equalTo: "#password"
                    }
                }
            });
        });
    </script>
    <?php } ?>
</body>

</html>