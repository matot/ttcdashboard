<script src="<?=base_url();?>assets/js/jquery-2.1.1.js"></script>
<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?=base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url();?>assets/js/instascan.min.js"></script>
<!-- Custom and plugin javascript -->
<script src="<?=base_url();?>assets/js/inspinia.js"></script>
<script src="<?=base_url();?>assets/js/plugins/pace/pace.min.js"></script>
<!-- Data picker -->
<script src="<?=base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<script>
$(function(){
    var current = window.location.href;
    console.log(current);
    $('#side-menu li a').each(function(){
        var $this = $(this);
        // if the current path is like this link, make it active
        if($this.attr('href')==current){
            $this.parent().addClass('active');
            if($this.parent().parent().hasClass('collapse')){
                $this.parent().parent().addClass('in');
            }
            
        }
    })
});
</script>