<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$app_name;?> - <?=@$title;?></title>
    <link rel="shortcut icon" href="favicon.png" />
    <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet">
</head>

<body>
<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="<?=base_url();?>assets/img/user-icon.png" width="50px" />
                             </span>
                        <a href="#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?=$auth->fullname;?></strong>
                             </span> <span class="text-muted text-xs block"><?=$auth->id_ttc;?></span> </span> </a>
                        
                    </div>
                    <div class="logo-element">
                        ADT
                    </div>
                </li>
                <li>
                    <a href="<?=base_url();?>"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">TTC</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">TTC Buaran</a></li>
                        <li><a href="#">TTC BSD </a></li>
                        <li><a href="#">TTC TB.Simatupang</a></li>
                        <li><a href="#">TTC Meruya</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Building Management</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?=base_url('bm/pemakaian-energi.php');?>"><i class="fa fa-fire"></i> Pemakaian Energi</a></li>
                        <li><a href="<?=base_url('bm/pemakaian-solar.php');?>"><i class="fa fa-fire"></i> Pemakaian Solar</a></li>
                        <li><a href="<?=base_url('bm/rekap-perizinan-gedung.php');?>"><i class="fa fa-fire"></i> Rekap Perijinan Gedung</a></li>
                       
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-info"></i> <span class="nav-label">Help</span></a>
                </li>
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="" class="form-control" name="top-search" id="top-search">
                </div>
            </form>
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Welcome to Automation Dashboard TTC.</span>
                </li>


                <li>
                    <a href="<?=base_url('login.php?act=logout');?>">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>

        </nav>
        </div>