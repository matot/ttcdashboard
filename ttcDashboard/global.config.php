<?php
    $db_name = "automation_ttc";

    $table_user_login = "adt_master_user";
    $table_pemakaian_energi = "adt_pemakaian_energi";
    $table_pemakaian_energi_item = "adt_pemakaian_energi_item";
    $table_pemakaian_solar = "adt_pemakaian_solar";
    $table_perizinan_gedung = "adt_perizinan_gedung";
    $table_log = "adt_log";
    

    $auth_name = "ADT_AUTH";
    $app_name = "Automation Dashboard TTC";

    function base_url($uri=null){
        $base_url = "http://localhost/ttcDashboard/";
        if($uri){
            return $base_url.$uri;
        }else {
            return $base_url;
        }
    }

    
?>