<?php
    session_start();
    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
    require_once("../global.config.php");
    require_once("../_lib/Connector.db.php");
    $db = new Db();
    require_once("../_lib/Helper.php");
    $helper = new Helper();

    if(!isset($_SESSION[$auth_name])){
        $helper->result_error(null,"Please login again.");
    }
    $mysqli = $db->connect('server_db',$db_name);
    date_default_timezone_set('Asia/Jakarta');
    // mb_language('uni'); 
    // mb_internal_encoding('UTF-8');
	// $mysqli->query("set names 'utf8'");


    
    function sanitate($val) {
        global $mysqli;
        return $mysqli->real_escape_string($val);
    }
    function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }

    $rawData = file_get_contents("php://input");
    $req = json_decode($rawData);
    if(!$req){
        $req = (object) $_POST;
    }

    $act = (isset($_GET['act'])) ? $_GET['act'] : null;
    $user = $_SESSION[$auth_name];
    switch ($act) {
        case 'simpan':
            if(!@$req->id_item || !@$req->kapasitas || !@$req->persentase || !@$req->pemakaian || !@$req->keterangan || !@$req->bulan_tahun){
                $helper->result(null,"Mohon isi field dengan benar",0);
            }
            $bulan_tahun = explode('-',$req->bulan_tahun,2);
            $bulan = $bulan_tahun[0];
            $tahun = $bulan_tahun[1];
            $id_ttc = $user->id_ttc;
            for($x=0;$x<count($req->id_item);$x++){
                $id_item = $req->id_item[$x];
                $kapasitas = $req->kapasitas[$x];
                $pemakaian = $req->pemakaian[$x];
                $persentase = $req->persentase[$x];
                $keterangan = $req->keterangan[$x];
                $mysqli->query("REPLACE INTO $table_pemakaian_energi (id_item,id_ttc,kapasitas,pemakaian,persentase,keterangan,bulan,tahun) VALUES ('$id_item','$id_ttc','$kapasitas','$pemakaian','$persentase','$keterangan','$bulan','$tahun')");
                if($mysqli->error){
                    $helper->result(null,$mysqli->error,0);
                }
            }
            $helper->result(true,"Sukses menyimpan data pemakaian energi.",1);
        break;
        case 'existing_data':
            if(!@$req->bulan_tahun){
                $helper->result(null,"Mohon isi field dengan benar",0);
            }
            $bulan_tahun = explode('-',$req->bulan_tahun,2);
            $bulan = $bulan_tahun[0];
            $tahun = $bulan_tahun[1];
            $id_ttc = $user->id_ttc;
            $query = "SELECT a.id, a.id_ttc,a.kapasitas,a.pemakaian,a.persentase,a.keterangan,a.bulan,a.tahun,b.id as id_item,b.kategori_item,b.nama_item,b.satuan_item
                            FROM $table_pemakaian_energi a 
                            JOIN $table_pemakaian_energi_item b ON a.id_item=b.id 
                            WHERE a.id_ttc='$id_ttc' AND a.bulan='$bulan' AND a.tahun='$tahun'
            ";
            $exec_query = $mysqli->query($query);
            $group_by_kategori_item = [];
            while($row=$exec_query->fetch_object()){
                $group_by_kategori_item["id_item_".$row->id_item]['id_item'] = $row->id_item;
                $group_by_kategori_item["id_item_".$row->id_item]['child'] = array(
                    "nama_item"=>$row->nama_item,
                    "satuan_item"=>$row->satuan_item,
                    "kapasitas"=>$row->kapasitas,
                    "pemakaian"=>$row->pemakaian,
                    "persentase"=>$row->persentase,
                    "keterangan"=>$row->keterangan
                    
                );
            }
            $helper->result($group_by_kategori_item,"Yeay..",1);
        break;
        case 'load_data':
            sleep(5);
            if(!@$req->bulan_tahun){
                $helper->result(null,"Mohon isi field dengan benar",0);
            }
            $bulan_tahun = explode('-',$req->bulan_tahun,2);
            $bulan = $bulan_tahun[0];
            $tahun = $bulan_tahun[1];
            $id_ttc = $user->id_ttc;
            $query = "SELECT a.id, a.id_ttc,a.kapasitas,a.pemakaian,a.persentase,a.keterangan,a.bulan,a.tahun,b.id as id_item,b.kategori_item,b.nama_item,b.satuan_item
                            FROM $table_pemakaian_energi a 
                            JOIN $table_pemakaian_energi_item b ON a.id_item=b.id 
                            WHERE a.id_ttc='$id_ttc' AND a.bulan='$bulan' AND a.tahun='$tahun'
            ";
            $exec_query = $mysqli->query($query);
            $group_by_kategori_item = [];
            while($row=$exec_query->fetch_object()){
                
                $group_by_kategori_item[$row->kategori_item]['child'][] = array(
                    "nama_item"=>$row->nama_item,
                    "satuan_item"=>$row->satuan_item,
                    "kapasitas"=>$row->kapasitas,
                    "pemakaian"=>$row->pemakaian,
                    "persentase"=>$row->persentase,
                    "keterangan"=>$row->keterangan
                );
            }
            $helper->result($group_by_kategori_item,"Yeay..",1);
        break;
        case 'load_data_grafik':
            $id_ttc = $user->id_ttc;
            $tahun = (@$_GET['tahun']) ? $_GET['tahun'] : date("Y");
            $query = "SELECT
                            nama_item,pemakaian
                        FROM
                            adt_pemakaian_energi a
                            JOIN adt_pemakaian_energi_item b ON a.id_item = b.id 
                        WHERE
                            a.id_ttc = '$id_ttc' AND
                            kategori_item = 'PEMAKAIAN LISTRIK' AND
                            tahun='$tahun'
                        GROUP BY tahun,bulan,nama_item";
            $exec_query = $mysqli->query($query);
            $output = [];
            $output_item = [];
            $color = ["#4472C4","#ED7D31","#4DE8C6","E548F2"];
            $last_item = "";
            while($row=$exec_query->fetch_object()){
                $output_item[$row->nama_item]["data"][] = (int) $row->pemakaian;
                if($last_item!=$row->nama_item){
                    $output_item[$row->nama_item]["name"] = $row->nama_item;
                }
                
                $last_item = $row->nama_item;
            }
            $n=0;
            $array_sum = [];
            foreach($output_item as $i=>$item){
                $array_sum = array_map(function () {
                    return array_sum(func_get_args());
                }, $array_sum, $item['data']);

                $object = (object) $item;
                $object->color=$color[$n];
                
                $output[] = $object;
                $n++;
            }
            $output[] = array("name"=>"Total","data"=>$array_sum,"color"=>"#A5A5A5");
            echo json_encode($output);
            
        break;
        default:
            $helper->result(null,"Mohon maaf, permintaan tidak dikenali",0);
        break;
    }
?>