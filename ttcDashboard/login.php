<?php
    session_start();
    require_once "global.config.php";

    if(isset($_SESSION[$auth_name]) && isset($_GET['act']) && $_GET['act']=='logout'){
        unset($_SESSION[$auth_name]);
        header('Location: login.php');
    }
    if(isset($_SESSION[$auth_name])){
        header('Location: index.php');
        exit();
    }
    $message = null;
    
    if(isset($_POST['username']) && isset($_POST['password']) && !empty($_POST['username']) && !empty($_POST['password'])){
        require_once("_lib/Connector.db.php");
        
        $db = new Db();
        $mysqli = $db->connect('server_db',$db_name);


        $ip = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
        $ip = $_SERVER['REMOTE_ADDR'];
        }
        $agent = $_SERVER['HTTP_USER_AGENT'];

        $username = $mysqli->real_escape_string($_POST['username']);
        $password = $mysqli->real_escape_string($_POST['password']);
        
        $query_check_user = $mysqli->query("SELECT * FROM $table_user_login WHERE username='$username'");
        if($query_check_user->num_rows===1){
            $row = $query_check_user->fetch_object();
            
            if(password_verify($password, $row->password)){
                $message = "Successfully login.";
                $_SESSION[$auth_name] = (object) array(
                    'username'=>$row->username,
                    'fullname'=>$row->fullname,
                    'privilege'=>$row->privilege,
                    'id_ttc'=>$row->id_ttc,
                    'created_at'=>$row->created_at,
                    'login_at'=>date("Y-m-d H:i:s")
                );
                $mysqli->query("INSERT INTO $table_log (type_log,user,`action`,reference) VALUES ('login','$row->username','login from $ip','$agent')");
                $mysqli->close();
                header('Location: index.php');
                exit();
            }else {
                $message = "Wrong Password.";
            }
        }else {
            $message = "User Not Found.";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$app_name;?> - Login</title>
    <link rel="shortcut icon" href="favicon.png" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
</head>
<body class="gray-bg">

    <div class="loginColumns animated fadeInDown">
        <div class="row">

            <div class="col-md-6" align="center">
                <h2 class="font-bold" style="margin:20px auto;">Welcome to <?=$app_name;?></h2>

                <img src="assets/img/4EB7BE73.png" width="170px" />


            </div>
            <div class="col-md-6">
                <div class="ibox-content">
                    <form class="m-t" role="form" action="login.php" method="post">
                        <?php
                            if(isset($message)){
                        ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $message; ?>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <input name="username" type="text" class="form-control" placeholder="Username" required="true">
                        </div>
                        <div class="form-group">
                            <input name="password" type="password" class="form-control" placeholder="Password" required="true">
                        </div>
                        <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                    </form>
                    
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright IT Support Business
            </div>
            <div class="col-md-6 text-right">
               <small>© 2019</small>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery-2.1.1.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</body>

</html>
