<?php
    session_start();
    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
    require_once("../global.config.php");
    require_once("../_lib/Connector.db.php");
    $db = new Db();
    require_once("../_lib/Helper.php");
    $helper = new Helper();

    if(!isset($_SESSION[$auth_name])){
        $helper->result_error(null,"Please login again.");
    }
    $mysqli = $db->connect('server_db',$db_name);
    date_default_timezone_set('Asia/Jakarta');
    // mb_language('uni'); 
    // mb_internal_encoding('UTF-8');
	// $mysqli->query("set names 'utf8'");

    
    function sanitate($val) {
        global $mysqli;
        return $mysqli->real_escape_string($val);
    }
    function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }

    $rawData = file_get_contents("php://input");
    $req = json_decode($rawData);
    if(!$req){
        $req = (object) $_POST;
    }

    $act = (isset($_GET['act'])) ? $_GET['act'] : null;
    $user = $_SESSION[$auth_name];
    switch ($act) {
        case 'simpan':
            $uraian_kerja =  $_POST['uraian_kerja'];
            $tanggal_pemakaian =  $_POST['tanggal_pemakaian'];
            $liter_pemakaian =  $_POST['liter_pemakaian'];
            $backup_time =  $_POST['backup_time'];
            $keterangan =  $_POST['keterangan'];

                if($liter_pemakaian == 00.00){
                    $helper->result(false,"inputan Liter salah, silahkan input data liter dengan benar.",0);
                    return false;
                }

                $mysqli->query("INSERT INTO $table_pemakaian_solar (uraian_kerja,tanggal_pemakaian,liter_pemakaian,backup_time,keterangan) 
                VALUES ('$uraian_kerja','$tanggal_pemakaian','$liter_pemakaian','$backup_time','$keterangan')");

                if($mysqli->error){
                    $helper->result(null,$mysqli->error,0);
                }   
                $helper->result(true,"Sukses menyimpan data pemakaian Solar.",1);

    break;
        case 'show_data':

            $query = "SELECT id_pemakaian_solar, uraian_kerja, tanggal_pemakaian, liter_pemakaian, backup_time,keterangan
            FROM $table_pemakaian_solar ORDER BY id_pemakaian_solar ";
        
        $exec_query = $mysqli->query($query);
            $group_by_uraian_kerja = [];
            while($row=$exec_query->fetch_object()){
                $group_by_uraian_kerja [] = array(
                        "id_pemakaian_solar"=>$row->id_pemakaian_solar,
                        "uraian_kerja"=>$row->uraian_kerja,
                        "tanggal_pemakaian"=>$row->tanggal_pemakaian,
                        "liter_pemakaian"=>$row->liter_pemakaian,
                        "backup_time"=>$row->backup_time,
                        "keterangan"=>$row->keterangan
                    );
            }
            
            $helper->result($group_by_uraian_kerja,"Yeay..",1);
           
    break;
        case 'update':
        alert("masuk update backend mam");
            $uraian_kerja =  $_POST['uraian_kerja'];
            $tanggal_pemakaian =  $_POST['tanggal_pemakaian'];
            $liter_pemakaian =  $_POST['liter_pemakaian'];
            $backup_time =  $_POST['backup_time'];
            $keterangan =  $_POST['keterangan'];
            $id_pemakaian_solar = $_POST['id_pemakaian_solar'];
            // $mysqli->query("REPLACE INTO $table_pemakaian_solar (uraian_kerja, tanggal_pemakaian, liter_pemakaian,keterangan, backup_time) VALUES('$uraian_kerja', '$tanggal_pemakaian',' $liter_pemakaian', '$keterangan', '$backup_time')");

            $mysqli->query("UPDATE $table_pemakaian_solar SET uraian_kerja = $uraian_kerja, tanggal_pemakaian = $tanggal_pemakaian, liter_pemakaian = $liter_pemakaian, backup_time = $backup_time, keterangan = $keterangan WHERE id_pemakaian_solar = $id_pemakaian_solar");
            if($mysqli->error){
                $helper->result(null,$mysqli->error,0);
            }
        $helper->result(true,"Sukses update pemakaian solar.",1);
    break;
    
        case 'delete':

        $id_pemakaian_solar = $_POST['id_pemakaian_solar'];
        $sqlquery = "DELETE FROM adt_pemakaian_solar where id_pemakaian_solar = '$id_pemakaian_solar'";
        $query = mysqli_query($mysqli, $sqlquery);
        $helper->result(true,"Sukses hapus pemakaian solar.",1);


    break;
        
        default:
            $helper->result(null,"Mohon maaf, permintaan tidak dikenali",0);
    break;
    }

  