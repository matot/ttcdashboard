/*
 Navicat Premium Data Transfer

 Source Server         : 10.47.150.143 Brili
 Source Server Type    : MySQL
 Source Server Version : 50632
 Source Host           : 10.47.150.143:3306
 Source Schema         : inventory_ttc

 Target Server Type    : MySQL
 Target Server Version : 50632
 File Encoding         : 65001

 Date: 23/07/2019 10:30:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for adt_master_user
-- ----------------------------
DROP TABLE IF EXISTS `adt_master_user`;
CREATE TABLE `adt_master_user`  (
  `username` char(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fullname` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` char(64) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `privilege` char(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_ttc` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of adt_master_user
-- ----------------------------
INSERT INTO `adt_master_user` VALUES ('admin', 'Administrator', '$2y$10$oTEQjYfWmkutUzY6z.6fluyxJmM3XHnRhZepHsbU2byk.gikKCoji', 'admin', 'ttc_bsd', '2019-07-02 09:07:58');
INSERT INTO `adt_master_user` VALUES ('ttc_bsd', 'TTC BSD', '$2y$10$oTEQjYfWmkutUzY6z.6fluyxJmM3XHnRhZepHsbU2byk.gikKCoji', 'admin', 'ttc_bsd', '2019-07-02 09:07:58');
INSERT INTO `adt_master_user` VALUES ('ttc_buaran', 'TTC BUARAN', '$2y$10$oTEQjYfWmkutUzY6z.6fluyxJmM3XHnRhZepHsbU2byk.gikKCoji', 'admin', 'ttc_buaran', '2019-07-02 09:07:58');
INSERT INTO `adt_master_user` VALUES ('ttc_meruya', 'TTC MERUYA', '$2y$10$oTEQjYfWmkutUzY6z.6fluyxJmM3XHnRhZepHsbU2byk.gikKCoji', 'admin', 'ttc_meruya', '2019-07-02 09:07:58');
INSERT INTO `adt_master_user` VALUES ('ttc_tbs', 'TTC TB.SIMATUPANG', '$2y$10$oTEQjYfWmkutUzY6z.6fluyxJmM3XHnRhZepHsbU2byk.gikKCoji', 'admin', 'ttc_tbs', '2019-07-02 09:07:58');

SET FOREIGN_KEY_CHECKS = 1;
