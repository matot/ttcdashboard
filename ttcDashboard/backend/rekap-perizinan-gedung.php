<?php
    session_start();
    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
    require_once("../global.config.php");
    require_once("../_lib/Connector.db.php");
    $db = new Db();
    require_once("../_lib/Helper.php");
    $helper = new Helper();

    if(!isset($_SESSION[$auth_name])){
        $helper->result_error(null,"Please login again.");
    }
    $mysqli = $db->connect('server_db',$db_name);
    date_default_timezone_set('Asia/Jakarta');
    // mb_language('uni'); 
    // mb_internal_encoding('UTF-8');
	// $mysqli->query("set names 'utf8'");

    
    function sanitate($val) {
        global $mysqli;
        return $mysqli->real_escape_string($val);
    }
    function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }

    $rawData = file_get_contents("php://input");
    $req = json_decode($rawData);
    if(!$req){
        $req = (object) $_POST;
    }

    $act = (isset($_GET['act'])) ? $_GET['act'] : null;
    $user = $_SESSION[$auth_name];
    switch ($act) {
        case 'simpan':
            $id_ttc =  $_POST['id_ttc'];
            $nama_perizinan =  $_POST['nama_perizinan'];
            $status_perizinan =  $_POST['status_perizinan'];
            $tahun_pembuatan =  $_POST['tahun_pembuatan'];
            $pemeriksaan1 =  $_POST['pemeriksaan1'];
            $pemeriksaan2 =  $_POST['pemeriksaan2'];
            $masa_berlaku1 =  $_POST['masa_berlaku1'];
            $masa_berlaku2 =  $_POST['masa_berlaku2'];
            $lama_masa_perizinan =  $_POST['lama_masa_perizinan'];

                $mysqli->query("INSERT INTO $table_perizinan_gedung(id_ttc, nama_perizinan, status_perizinan, tahun_pembuatan, pemeriksaan1, pemeriksaan2, masa_berlaku1, masa_berlaku2, lama_masa_perizinan) 
                VALUES ('$id_ttc','$nama_perizinan','$status_perizinan','$tahun_pembuatan','$pemeriksaan1','$pemeriksaan2','$masa_berlaku1','$masa_berlaku2','$lama_masa_perizinan')");

                if($mysqli->error){
                    $helper->result(null,$mysqli->error,0);
                }   
                $helper->result(true,"Sukses menyimpan data perizinan gedung.",1);

    break;
        case 'show_data':

            $query = "SELECT id_perizinan, id_ttc, nama_perizinan,status_perizinan,tahun_pembuatan,pemeriksaan1,pemeriksaan2,masa_berlaku1,masa_berlaku2,lama_masa_perizinan 
            FROM $table_perizinan_gedung ORDER BY id_perizinan ";
        
        $exec_query = $mysqli->query($query);
            $group_by_id_ttc = [];
            while($row=$exec_query->fetch_object()){
                $group_by_id_ttc [] = array(
                        "id_perizinan"=>$row->id_perizinan,
                        "id_ttc"=>$row->id_ttc,
                        "nama_perizinan"=>$row->nama_perizinan,
                        "status_perizinan"=>$row->status_perizinan,
                        "tahun_pembuatan"=>$row->tahun_pembuatan,
                        "pemeriksaan1"=>$row->pemeriksaan1,
                        "pemeriksaan2"=>$row->pemeriksaan2,
                        "masa_berlaku1"=>$row->masa_berlaku1,
                        "masa_berlaku2"=>$row->masa_berlaku2,
                        "lama_masa_perizinan"=>$row->lama_masa_perizinan,
                    );
            }
            
            $helper->result($group_by_id_ttc,"Yeay..",1);
           
    break;
        case 'update':
       
            $id_perizinan = $_POST['id_perizinan'];
            $id_ttc =  $_POST['id_ttc'];
            $nama_perizinan =  $_POST['nama_perizinan'];
            $status_perizinan =  $_POST['status_perizinan'];
            $tahun_pembuatan =  $_POST['tahun_pembuatan'];
            $pemeriksaan1 =  $_POST['pemeriksaan1'];
            $pemeriksaan2 =  $_POST['pemeriksaan2'];
            $masa_berlaku1 =  $_POST['masa_berlaku1'];
            $masa_berlaku2 =  $_POST['masa_berlaku2'];
            $lama_masa_perizinan = $_POST['lama_masa_perizinan'];
            $mysqli->query("REPLACE INTO $table_pemakaian_solar (id_ttc,uraian_kerja, tanggal_pemakaian, liter_pemakaian,keterangan, backup_time) VALUES('$uraian_kerja', '$tanggal_pemakaian',' $liter_pemakaian', '$keterangan', '$backup_time')");

            // $query = "UPDATE adt_perizinan_gedung SET id_ttc='$id_ttc',nama_perizinan='$nama_perizinan',status_perizinan='$status_perizinan',tahun_pembuatan='$tahun_pembuatan',pemeriksaan1='$pemeriksaan1',pemeriksaan2='$pemeriksaan2',masa_berlaku1='$masa_berlaku1',masa_berlaku2='$masa_berlaku2',lama_masa_perizinan='$lama_masa_perizinan' WHERE id_perizinan='$id_perizinan'";
            
            // $query = "UPDATE adt_perizinan_gedung SET id_ttc=$id_ttc,nama_perizinan=$nama_perizinan,status_perizinan=$status_perizinan,tahun_pembuatan=$tahun_pembuatan,pemeriksaan1=$pemeriksaan1,pemeriksaan2=$pemeriksaan2,masa_berlaku1=$masa_berlaku1,masa_berlaku2=$masa_berlaku2,lama_masa_perizinan=$lama_masa_perizinan WHERE id_perizinan='$id_perizinan'";
            $query = "REPLACE INTO adt_perizinan_gedung (id_ttc,nama_perizinan,status_perizinan,tahun_pembuatan,pemeriksaan1,pemeriksaan2,masa_berlaku1,masa_berlaku2,lama_masa_perizinan) VALUES('$id_ttc','$nama_perizinan','$status_perizinan','$tahun_pembuatan','$pemeriksaan1','$pemeriksaan2','$masa_berlaku1','$masa_berlaku2','$lama_masa_perizinan')";
            $sqlquery = mysqli_query($mysqli, $query);
            
            if($mysqli->error){
                $helper->result(null,$mysqli->error,0);
            }
            $helper->result(true,"Sukses update perizinan gedung.",1);
    break;

        case 'delete':
            
        $id_perizinan = $_POST['id_perizinan'];
        $sqlquery = "DELETE FROM adt_perizinan_gedung where id_perizinan = '$id_perizinan'";
        $query = mysqli_query($mysqli, $sqlquery);
        $helper->result(true,"Sukses hapus perizinan gedung.",1);

    break;
    
        default:
            $helper->result(null,"Mohon maaf, permintaan tidak dikenali",0);
    break;
    }

  