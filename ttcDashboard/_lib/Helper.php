<?php debug_backtrace() || die("Direct access not permitted");
date_default_timezone_set('Asia/Jakarta');

class Helper
{
    
    private function err_res($message)
    {
        return array("status" => 0, "message" => $message);
    }

    public function result($result = null, $message = null, $status = 1)
    {
        $data['result'] = $result;
        $data['message'] = $message;
        $data['status'] = $status;
        header("HTTP/1.1 200 OK");
        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
        exit();
    }

    public function result_null($result = null, $message = null)
    {
        $data['result'] = $result;
        $data['message'] = $message;
        $data['status'] = 0;
        header("HTTP/1.1 404 Not Found");
        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
        exit();

    }
    public function result_error($result = null, $message = null)
    {
        $data['result'] = $result;
        $data['message'] = $message;
        $data['status'] = 0; 
        header("HTTP/1.1 400 Bad Request");
        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
        exit();

    }
    public function user_info(){
        $ip = '';
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
        $ip = (isset($_SERVER['REMOTE_ADDR'])) ? $_SERVER['REMOTE_ADDR'] : null;
        }
        $agent = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : null;
        return (object) array("ip"=>$ip,"agent"=>$agent);
    }
}
