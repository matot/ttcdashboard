<?php
session_start();
require_once "../global.config.php";
if (!isset($_SESSION[$auth_name])) {
    header('Location: ' . base_url('login.php'));
}
$title = "Index";
$auth = (object) $_SESSION[$auth_name];
require_once "../component/header.php";
require_once "../_lib/Connector.db.php";

$db = new Db();
$mysqli = $db->connect('server_db', $db_name);
$item_pemakaian_energi = "SELECT * FROM $table_pemakaian_energi_item WHERE id_ttc='$auth->id_ttc'";
$ipe = $mysqli->query($item_pemakaian_energi);

?>

<div class="row">
    <div class="col-lg-3">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Filter </h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">      
                <div class="form-group">
                    <label>Bulan - Tahun</label>
                    <input type="text" name="filter_bulan_tahun" class="bulan_tahun form-control" size="6"
                        autocomplete="off" id="filter_bulan_tahun" placeholder="MM-YYYY" value="">
                </div>
                <div class="form-group">
                    <button class="btn btn-sm btn-info btn_tambah_data"
                        data-url="<?=base_url('backend/pemakaian-energi.php?act=existing_data');?>">Input Data</button>
                    <button class="btn btn-sm btn-success btn_load_data"
                        data-url="<?=base_url('backend/pemakaian-energi.php?act=load_data');?>">Load Data</button>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Rekap Data Pemakaian Energi <span class="title_bulan_tahun"></span></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>

                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: block;">
                <div class="container_rekap_pemakaian_energi"></div>

            </div>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">

            <div class="ibox-content">
                <div>
                    <h3 class="pull-left">Dashboard BM</h3>
                    <div class="pull-right">
                        <input type="text" class="tahun" value="<?=date('Y');?>" /><button
                            class="btn_filter_tahun">Filter</button>
                    </div>
                </div>
                <div id="container_listrik" style="clear:both;min-width: 310px; height: 400px; margin: 0 auto"></div>
            </div>
        </div>
    </div>
</div>


<!--MODAL AREA-->

<div class="modal inmodal" id="modalInputPemakaianEnergi" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInLeft">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h3 class="modal-title event_name">Input Rekap Pemakaian Energi <span class="title_bulan_tahun"></span>
                </h3>
            </div>
            <div class="modal-body">
                <form id="formInputPemakaianEnergi" method="post"
                    action="<?=base_url('backend/pemakaian-energi.php?act=simpan');?>">
                    <table class="table table-sm">
                        <?php
                        $last_kategori_item = "";
                        $element = "";
                        while ($row_item = $ipe->fetch_object()) {
                            $element .= '<tr class="id_item_'.$row_item->id.'">';
                            if ($last_kategori_item !== $row_item->kategori_item) {
                                $element .= "<td class='text-bold'>$row_item->kategori_item</td>";
                            } else {
                                $element .= "<td></td>";
                            }
                            $last_kategori_item = $row_item->kategori_item;
                            $element .= '
                                    <td>
                                        ' . $row_item->nama_item . '
                                    </td>
                                    <td width="140px">
                                        <input type="hidden" name="id_item[]" value="' . $row_item->id . '" />
                                        <div class="form-group">
                                            <label>Kapasitas *</label>
                                            <div class="input-group">
                                                <input name="kapasitas[]" type="number" class="form-control required"
                                                placeholder="" required="true">
                                                <span class="input-group-addon">' . $row_item->satuan_item . '</span>
                                            </div>

                                        </div>
                                    </td>
                                    <td width="140px">
                                        <div class="form-group">
                                            <label>Pemakaian *</label>
                                            <div class="input-group">
                                                <input name="pemakaian[]" type="number" class="form-control required"
                                                placeholder="" required="true">
                                                <span class="input-group-addon">' . $row_item->satuan_item . '</span>
                                            </div>

                                        </div>
                                    </td>
                                    <td width="130px">
                                        <div class="form-group">
                                            <label>Persentase *</label>
                                            <div class="input-group">
                                                <input name="persentase[]" type="number" class="form-control required"
                                                placeholder="" required="true">
                                                <span class="input-group-addon">%</span>
                                            </div>

                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <label>Keterangan *</label>
                                                <input name="keterangan[]" type="text" class="form-control required"
                                                placeholder="" required="true" value="-">
                                        </div>
                                    <td>
                                                    ';
                            $element .= "</tr>";
                        }
                        echo $element;
                        ?> 
                    </table>
                    <input type="hidden" name="bulan_tahun" id="bulan_tahun" />
                    <div class="form-group">
                        <button id="btnSubmitPemakaianEnergi" type="submit" class="btn btn-primary">Simpan Pemakaian
                            Energi</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>


<?php require_once "../component/assets_js.php";?>
<script src="<?=base_url();?>assets/plugins/highchart/highcharts.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/exporting.js"></script>
<script src="<?=base_url();?>assets/plugins/highchart/modules/export-data.js"></script>

<script type="text/javascript">
    $('document').ready(function () {
        $(".bulan_tahun").datepicker({
            format: "mm-yyyy",
            startView: "months",
            viewMode: 1,
            minViewMode: 1,
            autoclose: true
        });
        $(".tahun").datepicker({
            format: "yyyy",
            startView: "years",
            viewMode: 1,
            minViewMode: 2,
            autoclose: true
        });

        $(document).on("click", ".btn_tambah_data", function () {
            var bulan_tahun = $("#filter_bulan_tahun").val();
            $('#modalInputPemakaianEnergi input[type="number"],#modalInputPemakaianEnergi input[type="text"]').val("");
            if (bulan_tahun) {
                $('#modalInputPemakaianEnergi').modal('show')
                $("#bulan_tahun").val(bulan_tahun);
                $(".title_bulan_tahun").html(bulan_tahun);
                jQuery.ajax({
                    url: $(this).data('url'),
                    data: {
                        bulan_tahun: bulan_tahun
                    },
                    dataType: 'json',

                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    success: function (data) {
                        if (data.status == 1) {
                            var dataset = data.result;
                            //console.log(dataset);
                            $.each(dataset, function (index, item) {
                                //$("."+index+" input[name='id_item[]']").val(item.id_item);
                               
                                $("."+index+" input[name='kapasitas[]']").val(item.child.kapasitas);
                                $("."+index+" input[name='pemakaian[]']").val(item.child.pemakaian);
                                $("."+index+" input[name='persentase[]']").val(item.child.persentase);
                                $("."+index+" input[name='keterangan[]']").val(item.child.keterangan);
                            });
                            
                        }
                    },
                    error: function (e) {
                        alert(e.message);
                    }
                });
            } else {
                alert("Mohon untuk memilih bulan dan tahun untuk memulai input data");
            }

        });

        function transformDataset(index, item) {
            console.log(item.kategori);
        }
        //$(document).ajaxStart(function() { Pace.restart(); });
        $(document).on('click', '.btn_load_data', function (e) {
            e.preventDefault();
           
            var bulan_tahun = $("#filter_bulan_tahun").val();
            var url = $(this).data('url');
           
            if (bulan_tahun) {
                jQuery.ajax({
                    url: url,
                    data: {
                        bulan_tahun: bulan_tahun
                    },
                    dataType: 'json',
                    
                    method: 'POST',
                    type: 'POST', // For jQuery < 1.9
                    beforeSend:function(){
                        Pace.stop();
                        Pace.bar.render();
                    },
                    success: function (data) {
                     
                        Pace.start();
                        if (data.status == 1) {
                            var dataset = data.result;
                            console.log(dataset);
                            
                            var content =
                                "<table class='table table-bordered table-striped'>";
                            content += `
                                            <tr>
                                                <th>No</th>
                                                <th>Kategori</th>
                                                <th>Item</th>
                                                <th>Kapasitas</th>
                                                <th>Pemakaian</th>
                                                <th>Persentase</th>
                                                <th>Keterangan</th>
                                            </tr>
                            `;

                            var no = 1;
                            var total_kapasitas = 0,
                                total_pemakaian = 0,
                                total_persentase = 0;
                            $.each(dataset, function (index, item) {
                                var jumlah_child = item.child.length;
                                $.each(item.child, function (i, d) {
                                    total_kapasitas += parseInt(d
                                    .kapasitas);
                                    total_pemakaian += parseInt(d
                                    .pemakaian);
                                    total_persentase += parseFloat(d
                                        .persentase);
                                });
                                total_persentase = parseFloat(total_persentase /
                                    jumlah_child).toFixed(2);

                                content += `
                                        <tr>
                                                <td rowspan="${jumlah_child+1}">${no}</td>
                                                <td rowspan="${jumlah_child+1}">${index}</td>
                                                <td></td>
                                                <td>${total_kapasitas} ${item.child[0].satuan_item}</td>
                                                <td>${total_pemakaian} ${item.child[0].satuan_item}</td>
                                                <td>${total_persentase} %</td>
                                                <td></td>
                                            </tr>
                                `;
                                total_kapasitas = 0;
                                total_pemakaian = 0;
                                total_persentase = 0;
                                $.each(item.child, function (i, d) {
                                    console.log(d);
                                    

                                    content += `
                                            <tr>
                                                    <td class="text-bold">${d.nama_item}</td>
                                                    <td>${d.kapasitas} ${d.satuan_item}</td>
                                                    <td>${d.pemakaian} ${d.satuan_item}</td>
                                                    <td>${d.persentase} %</td>
                                                    <td>${d.keterangan}</td>
                                                </tr>
                                    `;
                                });
                                no++;
                            });
                            content += "</table>"
                            $('.container_rekap_pemakaian_energi').html(content);
                        }

                    },
                    error: function (e) {
                        Pace.start();
                        alert(e.message);
                    }
                });
            } else {
                alert("Mohon untuk memilih bulan dan tahun untuk menampilkan data");
            }
        });

        

        $(document).on('submit', 'form#formInputPemakaianEnergi', function (e) {
            e.preventDefault();

            var form = $(this);
            var data = new FormData(form[0]);

            //$.post('ajax.php?act=submit_event', form.serialize())
            jQuery.ajax({
                url: form.attr('action'),
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                method: 'POST',
                type: 'POST', // For jQuery < 1.9
                success: function (data) {
                    if (data.status == 1) {
                        location.reload();
                    }
                    alert(data.message);

                },
                error: function (e) {
                    alert(e.message);
                }
            });
        });

    });




    function loadDataGrafPemakaianListrik(tahun) {
        fetch('<?=base_url();?>backend/pemakaian-energi.php?act=load_data_grafik&tahun=' + tahun).then(function (
            response) {
            return response.json()
        }).then(function (data) {
            var chart_pemakaian_listrik = Highcharts.chart('container_listrik', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Grafik KWH Milik PLN'
                },
                // subtitle: {
                //     text: 'Source: WorldClimate.com'
                // },
                xAxis: {
                    categories: [
                        'Jan',
                        'Feb',
                        'Mar',
                        'Apr',
                        'May',
                        'Jun',
                        'Jul',
                        'Aug',
                        'Sep',
                        'Oct',
                        'Nov',
                        'Dec'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'KWH'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} kwh</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: data
            });
        });
    }
    var tahun = $(".tahun").val();
    loadDataGrafPemakaianListrik(tahun);
    $(".btn_filter_tahun").click(function () {

        loadDataGrafPemakaianListrik($(".tahun").val());
    });
</script>
<?php require_once "../component/footer.php";?>